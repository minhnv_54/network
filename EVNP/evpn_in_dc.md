# EVPN in Data Center

## Table of Contents
- [EVPN in Data Center](#evpn-in-data-center)
  - [Table of Contents](#table-of-contents)
  - [Preface](#preface)
  - [Chapter 1: Introduction to EVPN](#chapter-1-introduction-to-evpn)
  - [Chapter 2: Network Virtualization](#chapter-2-network-virtualization)
    - [Network Virtualization](#network-virtualization)
    - [Network Tunneling](#network-tunneling)
    - [VXLAN](#vxlan)
    - [Protocols to Implement the Control Plane](#protocols-to-implement-the-control-plane)
  - [Chapter 3: The Building Blocks of Ethernet VPN](#chapter-3-the-building-blocks-of-ethernet-vpn)
    - [A Brief History of EVPN](#a-brief-history-of-evpn)
    - [Architecture and Protocols for Traditional EVPN Deployment](#architecture-and-protocols-for-traditional-evpn-deployment)
    - [EVPN in the Data Center](#evpn-in-the-data-center)
    - [BGP Constructs for Virtual Networks](#bgp-constructs-for-virtual-networks)
      - [Address Family Indicator/Subsequent Address Family Indicator](#address-family-indicatorsubsequent-address-family-indicator)
      - [Route Distinguisher](#route-distinguisher)
      - [Route Target](#route-target)
      - [RD, RT, and BGP Processing](#rd-rt-and-bgp-processing)
      - [Encapsulation Method](#encapsulation-method)
      - [Route Types](#route-types)
      - [Other NLRI information](#other-nlri-information)
    - [Modifications to Support EVPN over eBGP](#modifications-to-support-evpn-over-ebgp)
      - [Keeping the NEXT HOP Unmodified](#keeping-the-next-hop-unmodified)
      - [Retaining Route Targets](#retaining-route-targets)
  - [Chapter 4: Bridging with EVPN](#chapter-4-bridging-with-evpn)
    - [An Overview of Traditional Bridging](#an-overview-of-traditional-bridging)
    - [Overview of Bridging with EVPN](#overview-of-bridging-with-evpn)
    - [What Ifs](#what-ifs)
    - [Handling BUM Traffic](#handling-bum-traffic)
      - [Ingress Replication](#ingress-replication)
      - [Underlay Multicast](#underlay-multicast)
      - [Dropping BUM Packets](#dropping-bum-packets)
      - [Signaling Which Approach Is Employed](#signaling-which-approach-is-employed)
    - [Handling MAC Moves](#handling-mac-moves)
    - [Support for Dual-Attached Hosts](#support-for-dual-attached-hosts)
    - [ARP/ND Suppression](#arpnd-suppression)
  - [Chapter 5: Routing in EVPN](#chapter-5-routing-in-evpn)
    - [The Case for Routing in EVPN](#the-case-for-routing-in-evpn)
    - [Routing Models](#routing-models)
    - [Where Is the Routing Performed?](#where-is-the-routing-performed)
      - [Centralized Routing](#centralized-routing)
      - [Distributed Routing](#distributed-routing)
    - [How Routing Works in EVPN?](#how-routing-works-in-evpn)
      - [Asymmetric Routing](#asymmetric-routing)
      - [Symetric Routing](#symetric-routing)
      - [Comparison between symmetric and asymmetric routing](#comparison-between-symmetric-and-asymmetric-routing)
    - [Configuring and Administering EVPN](#configuring-and-administering-evpn)
    - [Considerations for Deploying EVPN in Large Networks](#considerations-for-deploying-evpn-in-large-networks)
  - [References](#references)

## Preface

Tài liệu này chủ yếu dựa trên cuốn sách "EVPN in the Data Center" của Dinesh G. Dutt - đồng tác giả của VXLAN, với mục đích cung cấp cho người đọc cái nhìn tổng quan nhất về EVPN/VXLAN trong data center networks.

Nhiều nguồn tài liệu tham khảo, trong  đó chủ yếu là các tài liệu chuẩn RFC và docs của các vendor lớn như Cisco, Arista và Juniper, cũng được tận dụng để làm rõ thêm những khái niệm chưa được đề cập đến trong cuốn sách.

Rất mong nhận được sự góp ý, bổ sung của bạn đọc để giúp tài liệu trở nên hoàn thiện.

## Chapter 1: Introduction to EVPN

Xu hướng chuyển dịch hạ tầng mạng từ L2 sang L3 network dần trở nên phổ biến trong môi trường data center. Một vấn đề nhanh chóng nảy sinh sau quá trình chuyển dịch là nhiều dịch vụ hiện tại vẫn hoạt động dựa trên kết nối L2 truyền thống và cần có một giải pháp cung cấp kết nối L2 trên nền tảng hạ tầng L3 cho chúng. EVPN sinh ra để giải quyết vấn đề này.

Một cách đơn giản nhất, EVPN là công nghệ cho phép kết nối các phân mảnh mạng L2 bị chia cách bởi môi trường L3 network bằng cách xây dựng một môi trường mạng L2 ảo, phủ lên (overlay) hạ tầng mạng L3. EVPN thực hiện nhiệm vụ này với sự trợ giúp của giao thức điều khiển BGP.

EVPN là công nghệ được phát triển từ khá lâu và ứng dụng rộng rãi trong môi trường mạng MPLS. Quá trình tích hợp nó với môi trường VXLAN-based IP network cũng được diễn ra một cách nhanh chóng và nhất quán giữa các network vendors lớn với những bản triển khai tương đối ổn định.

Vấn đề đối với EVPN đó là nó là một công nghệ phức tạp với nhiều khái niệm, một vài trong số đó có thể không phù hợp với những đặc trưng của DC networks. Trong tài liệu này, tác giả sẽ cố gắng giải thích một cách đơn giản nhất về EVPN trong môi trường DC bằng cách sử dụng các khái niệm chung nhất, không phụ thuộc vào các khái niệm của vendors.

Cuốn sách bắt đầu bằng hai nền tảng cơ bản nhất của EVPN: ảo hóa network và sự du nhập của BGP trong môi trường DC. Tiếp đó, ta sẽ tìm hiểu đến bridging và routing trong EVPN. Quản lý và cấu hình EVPN sẽ được đề cập tiếp theo. Và cuối cùng là những cân nhắc khi triển khai EVPN trong môi trường DC quy mô lớn.

## Chapter 2: Network Virtualization

### Network Virtualization

Mục tiêu: Chia sẻ hạ tầng vật lý giữa các virtual networks, đồng thời cô lập các virtual networks với nhau

Phân biệt các dạng ảo hóa network dựa trên các tính chất:
-   Tầng hoạt động trong chồng giao thức OSI: L2 or L3 virtual network
-   Cách thức định danh virtual network: Dùng trường identifier trong tiêu đề gói tin(VLAN, L3VPN và VXLAN) hay dựa trên cổng nhận gói tin và tiêu đề (plain VRF)
-   Thiết bị chuyển tiếp gói tin có biết đến sự tồn tại của virtual network không: Overlay (Không, ví dụ như VXLAN, L3VPN) và Non-overlay (Có, ví dụ như VLAN, VRF)

Lợi ích của overlay virtual network:
-   Scale better: Thiết bị trung gian không cần lưu trữ trạng thái của VNs
-   Rapid provisioning VNs: Chỉ cấu hình trên các edge devices trực tiếp kết nối với VNs, không cần cấu hình trên thiết bị trung gian
-   Reusable: Khi có sự thay đổi hoặc nâng cấp, chỉ các edge devices phải chịu tác động, các thiết bị khác vẫn giữ nguyên trạng thái

### Network Tunneling

Tunneling là phương thức chuyển tiếp dữ liệu phổ biến trong overlay networks.

Ý tưởng chung của các giải pháp tunnelling là đóng gói hai lớp:
-   Thiết bị nguồn gửi traffic tới địa chỉ đích nằm trong cùng virtual network
-   Edge device (hay NVE - network virtualization edge) gắn với thiết bị nguồn đóng gói thêm một lớp bên ngoài có địa chỉ nguồn là địa chỉ underlay của nó, địa chỉ đích là địa chỉ underlay của edge device gắn với thiết bị đích
-   Các thiết bị trung gian chuyển tiếp gói tin tới edge device đích trong mạng underlay như thông thường
-   Edge device đích nhận gói tin, bóc tách gói tin để lấy về gói tin overlay, cuối cùng chuyển tiếp gói tin tới virtual address  của thiết bị đích

Các tính chất của tunnels:
-   Giao thức đóng gói:
    -   L2: Double VLAN, TRILL, MAC-in-MAC, ...
    -   L3: VXLAN, GRE và MPLS
-   Nội dung payload: L2 frames (L2 tunnel) hay L3 packets (L3 tunnel)
-   Dạng kết nối: điểm-điểm (L3VPN with MPLS) hoặc điểm-đa điểm (VPN Switching)
-   Kích thước của VNI: MPLS dùng 20-bit MPLS Labels, trong khi các giải pháp khác dùng 24-bit VNIs

Các hạn chế của giải pháp tunnelling
-   Load balancing:
    -   Thông thường, khi có nhiều đường đi giữa nút nguồn và đích, hệ thống mạng phải đưa ra quyết định lựa chọn đường đi nào là phù hợp nhất với từng flow. Cách thức lựa chọn đường đi có thể là chọn đường đi cố định (để đảm bảo tính nhất quán) hay ngẫu nhiên (để phân tải)
    -   Nếu sử dụng tunnel, mọi giao tiếp giữa các endpoints ở đằng sau một cặp edge device đều được coi như là một flow giữa cặp edge device này, làm mất đi khả năng cân bằng tải cho các flow overlay
    -   Cách giải quyết vấn đề này là sử dụng tunnel UDP, trong đó địa chỉ cổng nguồn của gói tin underlay được xác định bằng một giá trị băm của tiêu đề của gói tin overlay. Thao tác này vừa giúp phân tải đối với các gói tin thuộc về các flow khác nhau, vừa giúp cố định đường đi trên một flow.
-   NIC behaviour:
    -   Trên interfaces của các compute nodes thường có các cơ chế tăng cường hiệu năng bằng cách ủy nhiệm (offload) tính năng phân mảnh TCP hay tính checksum cho các gói tin nhằm giảm overhead cho CPU
    -   Nếu ta sử dụng tunnel, interface chỉ có thể offload được gói tin ngoài mà không thể offload gói tin overlay, dẫn đến việc CPU vẫn bị quá tải
    -   Giải pháp là sử dụng NIC đời mới hỗ trợ offload cho tunnelled packets
-   MTU: Thêm tiêu đề làm giảm kích thước payload có thể truyền đi
-   Lack of visibility: Khó trouble shoot khi các công cụ như traceroute không thể phát hiện đường đi của gói tin bên ngoài mạng underlay

### VXLAN

VXLAN là giải pháp tunnelling chạy trên IP networks giúp cung cấp kết nối L2 cho các endpoints (gói tin overlay là Ethernet frame). VXLAN sử dụng cơ chế đóng gói UDP giúp cân bằng tải traffic giữa NVEs, hay còn gọi là các VTEPs (VXLAN Tunnel Endpoints).

Khuôn dạng tiêu đề VXLAN như sau:
![](https://learning.oreilly.com/library/view/evpn-in-the/9781492029045/assets/evpn_0203.png)

VXLAN cung cấp kết nối điểm - đa điểm, gói tin multicast hay broadcast có thể được gửi từ một VTEP tới nhiều VTEPs khác trong network.

Trong những năm gần đây, xu hướng chuyển dịch từ hạ tầng network từ L2 lên L3, cũng như xu hướng thay thế dần mô hình mạng DC 3 lớp thành mô hình 2 lớp đã tạo tiền đề cho sự phát triển ngày càng mạnh mẽ của VXLAN trong môi trường mạng DC.

### Protocols to Implement the Control Plane

Control plane trong overlay networks thực hiện hai nhiệm vụ chính:
1.  Cung cấp cơ chế giúp edge devices học được địa chỉ overlay nào nằm sau edge device đích nào
1.  Cho phép các edge devices trao đổi thông tin các VNs mà chúng quan tâm đến, phục vụ giao tiếp điểm - đa điểm

VXLAN sử dụng ánh xạ `(VNI, MAC) -> VTEP's IP` lưu trữ trong MAC forwarding table để thực hiện dịch địa chỉ.

Thiết kế của VXLAN cho phép các compute nodes có thể trở thành NVE, điều này có hai tác dụng:
1.  Không cần cấu hình EVPN/VXLAN trên hạ tầng network vật lý
2.  Vị trí cũng như thông tin địa chỉ MAC của các VMs luôn được biết bởi compute node mà không cần các giải pháp học MAC truyền thống trong L2 networks

Tận dụng lợi ích thứ 2, nhiều giải pháp SDN đã được đề xuất để trở thành control plane, giúp trao đổi thông tin định tuyến overlay giữa các compute node. Tuy nhiên, vì nhiều lý do, phần lớn các giải pháp này đều không đạt được hiệu quả như mong đợi.

Một hướng tiếp cận khác là sử dụng BGP để triển khai control plane, đại diện bởi EVPN. Cách tiếp cận này được gọi là controller-less VXLAN và đang ngày càng trở nên phổ biến.

*Side note*:
1.  Giải pháp SDN Tungsten Fabric thiết kế control plane dựa trên EVPN, vừa tận dụng được hiệu quả của controller tập trung, vừa tương thích với hạ tầng overlay điều khiển bởi EVPN
2.  Để ám chỉ overlay virtual networks, tài liệu này sử dụng các khái niệm "virtual network", "VNI" và "VRF" một cách tương đồng

## Chapter 3: The Building Blocks of Ethernet VPN

### A Brief History of EVPN

Các giao thức mạng riêng ảo (VPN - Virtual Private Networks) có mục đích chung là kết nối nhiều private networks trên môi trường public network. Như đã đề cập ở chương 2, kết nối VPN có thể diễn ra ở L2 hoặc L3. Các giải pháp L2 VPNs, ví dụ như VPLS, vốn chỉ mô phỏng lại hành vi của mạng L2 truyền thống: flood unknown packets trên cây khung tạo ra bởi STP. EVPN được sinh ra để giải quyết những nhược điểm cố hữu của STP cũng như hành vi flood and learn diễn ra tại L2, đồng thời cung cấp khả năng định tuyến L3 giữa các virtual networks. Nhờ vậy, EVPN trở thành một giải pháp tân tiến trong họ các giải pháp L2/L3 VPNs, giúp tạo ra các mạng riêng ảo dành riêng cho khách hàng với khả năng cô lập và định tuyến traffic một cách tối ưu nhờ sự trong suốt giữa network overlay với hạ tầng transport network vật lý.

Giao thức tiền nhiệm của EVPN là Over-the-Top Virtualization (OTV) được đề xuất bởi Cisco. Nó sử dụng giao thức IS-IS làm control plane và chạy trên môi trường mạng IP.

EVPN được đề xuất sau đó bởi Juniper và được định nghĩa trong tài liệu [RFC7432](https://tools.ietf.org/html/rfc7432). Ban đầu EVPN chạy trên hạ tầng MPLS, vốn là hạ tầng mạng được sử dụng phổ biến bởi các nhà cung cấp dịch vụ (service provider - SP) và sử dụng BGP là control protocol. Nhờ việc hỗ trợ hạ tầng MPLS, EVPN nhanh chóng được tiếp nhận bởi nhiều SPs khác nhau và trở thành tiêu chuẩn phổ biến.

Cuối cùng, do sự phát triển nhanh chóng của VXLAN trong môi trường mạng data centers, EVPN được tích hợp để cung cấp giải pháp mạng riêng ảo cho hạ tầng IP/VXLAN trong data centers. [RFC8365](https://tools.ietf.org/html/rfc8365) đề xuất một vài những thay đổi đối với EVPN để nó trở nên phù hợp hơn với DC networks.

### Architecture and Protocols for Traditional EVPN Deployment

Các border router của khách hàng (ký hiệu CE - Customer Edge) kết nối với các router biên của nhà cung cấp dịch vụ (ký hiệu PE - Provider Edge) để sử dụng các dịch vụ mạng riêng ảo. Các PEs thuộc cùng một nhà cung cấp thường thuộc cùng một hệ tự trị (Autonomous System - AS), vì vậy chúng kết nối với nhau sử dụng các giao thức định tuyến nội vùng như OSPF hay IS-IS. Sau đó PEs trao đổi với nhau thông tin L2 addresses học được từ CEs mà chúng kết nối tới, qua đó cung cấp kết nối L2 VPN cho khách hàng.

![image](/uploads/b996f7f897d393f677d959db1133bb72/image.png)

Kết nối IBGP peering giữa các PEs phải có topo dạng full mesh: Mỗi PE phải kết nối với tất cả các PEs còn lại. Mô hình này rõ ràng không có khả năng mở rộng tốt, bởi vậy các SP thường sử dụng các router trung gian giữa các PEs đóng vai trò là route reflector (RRs), quảng bá routes gửi tới từ một PE tới các PEs còn lại.

![Route Reflector](https://www.researchgate.net/publication/265788731/figure/fig1/AS:646797840097284@1531220030535/Explanation-of-Route-Reflector.png)

Thông thường, EVPN trong môi trường mạng SP sử dụng IBGP làm control plane và một giao thức khác phục vụ định tuyến giữa các IBGP peers.

### EVPN in the Data Center

Trong môi trường mạng data center, kiến trúc Clos đang trở thành kiến trúc mạng tiêu chuẩn. Topo mạng Clos bao gồm hai lớp: Lớp leaf chứa các Top-of-rack (TOR) switches đóng vai trò tương tự như PEs trong môi trường mạng EVPN SP. Lớp spine thực hiện nhiệm vụ kết nối các router thuộc lớp leaf. Topo mạng Clos được mô phỏng bởi hình dưới:

![image](/uploads/0d022d2b3f2d66da329c98a1ce4eb188/image.png)

Có nhiều cách lựa chọn giao thức để implement các thủ tục định tuyến trong EVPN:
-   Cách 1: Sử dụng giải pháp tương tự như SP EVPN: (1) Các leaf swictches kết nối đến nhau bởi một giao thức IGP routing: OSPF hoặc IS-IS, (2) Các leaf switches trao đổi overlay route trên các iBGP session và (3) Các spine switches đóng vai trò là route reflector cho EVPN routes giữa các leaf
-   Cách 2: Sử dụng eBGP để định tuyến underlay: (1) Các leaf switches thuộc về các AS khác nhau và kết nối với nhau thông qua eBGP, (2) Thao tác trao đổi overlay routes vẫn được thực hiện trên iBGP sessions và (3) Các spine switches vẫn là route reflector cho EVPN routes
-   Cách 3: Sử dụng eBGP để định tuyến cả overlay và underlay: (1) Thông tin định tuyến overlay và underlay được quảng bá trên cùng một eBGP session, (2) Không cần cấu hình spine switches trở thành route reflector cho EVPN routes vì eBGP cho phép các peers chuyển tiếp BGP routes cho nhau. Cách này là đơn giản nhất nhưng không phải vendor nào cũng hỗ trợ.

Trong tài liệu này, nếu không nói gì thêm, bạn đọc có thể ngầm hiểu giao thức định tuyến overlay là iBGP và giao thức định tuyến underlay có thể là bất kỳ giao thức nào.

### BGP Constructs for Virtual Networks

Mục này phân tích vai trò của BGP trong việc triển khai control plane cho EVPN trong data centers.

Để một giao thức điều khiển có khả năng trao đổi thông tin của một virtual network, nó cần hỗ trợ các cơ chế để xác định:
1.  Họ địa chỉ của prefix cần trao đổi (Address Family Indicator/Subsequent Address Family Indicator - AFI/SAFI)
1.  Route thuộc virtual network nào (thông qua Route Distinguisher (RD) và Route Target (RT))
1.  Phương thức đóng gói trên tunnel overlay (thông qua một bản ghi Extended Community trong bản tin BGP)

Bên cạnh đó, giao thức điều khiển cần phải có cấu trúc thông điệp trao đổi route phù hợp với tiêu chuẩn của BGP.

Các mục nhỏ tiếp theo sẽ đề cập đến từng vấn đề liệt kê ở trên.

#### Address Family Indicator/Subsequent Address Family Indicator

[RFC 4760](https://tools.ietf.org/html/rfc4760) định nghĩa cách thức cho phép BGP quảng bá route cho nhiều loại địa chỉ khác nhau trong một BGP session, bao gồm địa chỉ IP, MAC hay MPLS label.

Address Family Indicator (AFI) dùng để chỉ ra họ địa chỉ của route được quảng bá là gì. Ví dụ IPv4 có AFI bằng 1, IPv6 có AFI bằng 2, 48-bit MAC có AFI bằng 16389, v.v.. Danh sách AFI đầy đủ tại [đây](https://www.iana.org/assignments/address-family-numbers/address-family-numbers.xhtml)

Trong mỗi AFI, thông tin Subsequent Address Family Indicator (SAFI) thường được dùng để chỉ ra cụ thể hơn về loại địa chỉ của route (ví dụ: multicast hay unicast). Danh sách SAFI đầy đủ tại [đây](https://www.iana.org/assignments/safi-namespace/safi-namespace.xhtml)

Thông tin AFI/SAFI được gửi đi trong bản tin BGP OPEN, các peers sẽ chỉ thiết lập kết nối BGP tới peer nào hỗ trợ AFI/SAFI giống như nó. Đối với EVPN, thông tin AFI/SAFI là `l2vpn/evpn`.

#### Route Distinguisher

Do các virtual networks có thể dùng chung dải địa chỉ, cần có một cơ chế giúp BGP phân biệt địa chỉ thuộc về các virtual network khác nhau trong các bản tin quảng bá route khác nhau. Đây là nhiệm vụ của Route Distinguisher (RD).

RD là một chuỗi 8-byte được gắn vào trước mỗi địa chỉ được quảng bá bởi BGP, giúp địa chỉ này từ locally-unique trên virtual network trở thành globally-unique. RD có ba loại format, tuy nhiên format dưới đây là được đề xuất bởi EVPN:

```
+---------------------+----------------------------------------+-----------------------+
|      Type = 1       |          IPv4 loopback address         |  Distinct among VNIs  |
|      (2 bytes)      |               (4 bytes)                |        (2 bytes)      |
+---------------------+----------------------------------------+-----------------------+
```

Vấn đề nảy sinh là VNI có kích thức 24 bits (3 bytes), trong khi kích thước trường thứ 3 (2 bytes) là quá nhỏ để tạo mapping 1:1 tới tất cả các VNIs. Tuy nhiên trong thực tế các router thường không support quá 64000 VXLAN và cũng không nên support số lượng quá lớn VXLAN vì khi một router down có thể làm ảnh hưởng tới rất nhiều khách hàng.

#### Route Target

Các bản tin quảng bá BGP có thể có các thông tin *path attributes* đi kèm. Các thuộc tính này có thể hiểu như những chú ý đính kèm khi quảng bá route, giống như những miếng note được dán trên kiện hàng với những thông điệp kiểu như là "hàng dễ vỡ, xin nhẹ tay", hay "không được gấp", v.v..

Có nhiều loại path attribute phổ biến, bao gồm *attribute*, *community* và *extended community*. Bạn đọc có thể tự tìm hiểu thêm về những loại thuộc tính này trên internet.

Mục này đề cập đến một path attribute quan trọng trong EVPN: *Route Target* (RT). RT là một thuộc tính có chức năng "mã hóa" virtual network mà nó đại diện. Trên mỗi BGP router, tương ứng với mỗi virtual network có ít nhất một *import RT* và một *export RT*, trong đó:
-   Export RT: Được thêm vào *extended community* trong thông điệp quảng bá route EVPN mà router gửi đi
-   Import RT: Router chỉ cập nhật bảng định tuyến EVPN từ thông điệp quảng bá route mà nó nhận được nếu trường RT trong thông điệp này trùng với import RT được cấu hình trên virtual network

Tóm lại, các routes trong cùng một virtual network trên hai routers chỉ có thể quảng bá được cho nhau nếu export RT trên virtual network của router gửi trùng với import RT trên virtual network của router nhận.

Chú ý:
-   Import và export RT phải được cấu hình trên tất cả các virtual network
-   Một virtual network có thể có nhiều import và export RT. Logic xử lý mặc định trong trường hợp này như sau:
    -   Tất cả export RTs đều được đính kèm vào thông điệp quảng bá route EVPN mà router gửi đi
    -   Router sẽ import route nếu một trong số import RTs trùng với RT trên thông điệp quảng bá route

RT cũng có kích thước 8 bytes và có thể được sinh ra một cách tự động. Trước khi tìm hiểu về cách sinh RT tự động, hãy tìm hiểu qua về cấu trúc của một trường extended community tổng quát:

```
+------------+------------+-----------------------------+-----------------------------+
|    Type    |  Sub Type  |     Global Administrator    |     Local Administrator     |
|  (1 byte)  |  (1 byte)  |        (2 or 4 bytes)       |        (4 or 2 bytes)       |
+------------+------------+-----------------------------+-----------------------------+
```

Trong đó:
-   Type: Là một trong các types được liệt kê tại [đây](https://www.iana.org/assignments/bgp-extended-communities/bgp-extended-communities.xml)
-   Sub type: Cho biết community chứa thông tin gì, với mỗi Type ở trên có một list các sub types tương ứng
-   Global Administrator: Giá trị độc nhất giữa các routers
-   Local Administrator: Giá trị độc nhất trên một router

RT có thể là community loại 1 hoặc loại 3, trong đó:
-   Type = 1: Sử dụng 2-byte ASN làm GA và 4-byte LA
-   Type = 3: Sử dụng 4-byte ASN làm GA và 2-byte LA

Tuy nhiên, chỉ RT loại 1 mới có thể được sinh một cách tự động, format của RT được sinh tự động trong môi trường EVPN như sau:

```
     +-----------------+-----------------+-----------------------------------+
     |   Type = 0x01   | Sub Type = 0x02 |            AS Number              |
     |    (1 byte)     |    (1 byte)     |            (2 bytes)              |
     +-----------------------------------+-----------------------------------+
     |     Flags       |                    Service ID                       |
+--- |    (1 byte)     |   (E.g.: 24-bit VXLAN or 12 bits 0 + 12-bit VLAN)   |
|    +-----------------+-----------------------------------------------------+
|
|
|    +--------+--------------------------+-----------------------------------+
+--> |   A    |           Type           |             Domain ID             |
     |(1 bit) |         (3 bits)         |             (4 bits)              |
     +--------+--------------------------+-----------------------------------+
```

Trong đó:
-   Sub Type = 2: Community này là route target
-   Flags:
    -   A: Bằng 0 nếu RT được sinh tự động, 1 nếu là thủ công
    -   Type: Loại virtual network
        -   0 : VID (802.1Q VLAN ID)
        -   1 : VXLAN
        -   2 : NVGRE
        -   3 : I-SID
        -   4 : EVI
        -   5 : dual-VID (QinQ VLAN ID)
    -   Domain ID: Định danh cho identifier space (ví dụ trong trường hợp VNI space overlap)
-   Service ID: Là định danh của virtual network tương ứng với cờ Type ở trên, thường là VNI

#### RD, RT, and BGP Processing

Từ những gì tìm hiểu được ở trên, ta có thể thấy RD và RT đều đại diện cho một virtual network, vậy tại sao lại cần cả hai thứ trong một thông điệp quảng bá route EVPN?

Để trả lời câu hỏi này, trước hết ta hãy ghi nhớ rằng RD được thêm vào prefix của route để đảm bảo được các route trong bảng định tuyến BGP là duy nhất, trong khi RT được dùng để trao đổi route giữa các virtual networks trên các routers. Vấn đề đặt ra ở đây là liệu dùng chung hai khái niệm này có được không? Nếu RT kiêm luôn chức năng của RD thì có vấn đề gì? Nếu RD kiêm luôn chức năng của RT thì có vấn đề gì?

Thứ nhất, RD không thể kiêm luôn chức năng của RT. Lý do đầu tiên là một RD không thể đại diện cho nhiều RTs. Lý do thứ 2 là thiết kế của RT giúp router có quyền quyết định có import route gửi từ một router khác hay không, nếu chỉ sử dụng RD thì mỗi router phải import route từ mọi routers khác trong cùng một virtual network. Nhìn chung, do chức năng của RT linh hoạt hơn RD nên không thể dùng RD thay cho RT được.

Trong ví dụ dưới đây, cấu hình RTs khiến cho router A và C chỉ có thể trao đổi route một cách gián tiếp qua router B mà không thể làm điều đó một cách trực tiếp. Dùng RD không cho phép ta thực hiện được điều này.

```
+-------------------+               +-------------------+
|      Router A     |-------------->|      Router B     |
| import: 64512:124 |               | import: 64512:123 |
| export: 64512:123 |<--------------| export: 64512:124 |
+-------------------+               +-------------------+
                                             |   ^
                                             |   |
                                             |   |
                                             |   |
                                             v   |
                                    +-------------------+
                                    |      Router C     |
                                    | import: 64512:124 |
                                    | export: 64512:123 |
                                    +-------------------+
```

Thứ hai, RT cũng không kiêm được chức năng của RD, do nếu không dùng RD thì prefix từ các router khác nhau có thể giống nhau. Khi nhận được route mới có cùng prefix với route cũ nhưng có RT khác, router sẽ không thể biết rằng đây là route mới hay là route cũ có thông tin RT thay đổi. Bởi vậy, RT sẽ không thể đảm nhận chức năng của RD: đảm bảo tính duy nhất của route.

#### Encapsulation Method

Trên bản tin quảng bá BGP cũng cần có một *path attribute* để chỉ ra phương thức đóng gói mà router sử dụng là gì. Thuộc tính này cũng là một extended community, tên là *Tunnel Encapsulation*, được định nghĩa trong [RFC 8365](https://tools.ietf.org/html/rfc8365), nhằm mở rộng khả năng hỗ trợ giao thức đóng gói khác ngoài MPLS.

Một vài kiểu tunnel được hỗ trợ:
```
Value    Name
-----    ------------------------
8        VXLAN Encapsulation
9        NVGRE Encapsulation
10       MPLS Encapsulation
11       MPLS in GRE Encapsulation
12       VXLAN GPE Encapsulation
13       MPLS in UDP Encapsulation
```

#### Route Types

Thông điệp BGP UPDATE mang theo thông tin reachability cho từng loại prefix. Thông tin này được đóng trong một cấu trúc gọi là Network Layer Reachability Information (NLRI). Với các loại AFI/SAFI thông thường, mỗi bản tin UPDATE chứa thông tin NLRI cho cùng một loại prefix.

Nhưng EVPN thì khác, bản tin BGP UPDATE có thể chứa thông tin định tuyến cho một địa chỉ MAC hoặc là cả một virtual network. Hơn nữa, thông thường việc xác định địa chỉ là unicast hay multicast được thực hiện thông qua SAFI, tuy nhiên do EVPN đã dùng cả AFI và SAFI (AFI = `l2vpn`, SAFI = `evpn`) rồi, nó thực hiện công việc này thông qua `Route Type`. Các route EVPN cơ bản như sau:

Route Type | What it carries                 | Primary use                                                                                   |
-----------|---------------------------------|-----------------------------------------------------------------------------------------------|
Type 1     | Ethernet Segment Auto Discovery | Used in the data center in support of multihomed endpoints                                    |
Type 2     | MAC, VNI, IP                    | Advertises reachability to a specific MAC address, and optionally its IP address              |
Type 3     | VNI/VTEP Association            | Advertises reachability in a virtual network                                                  |
Type 4     | Multicast Information           | Supports multihomed endpoints, ensures that only one of the VTEPs forwards multicast packets  |
Type 5     | IP Prefix, L3 VNI               | Advertises prefix (not /32 or /128), routes such as summarized routes in a virtual L3 network |
Type 6     | Multicast group membership info | Information about interested multicast groups derived from IGMP                               |

Trong đó chỉ có Type 2, 3 và 5 là bắt buộc, các loại routes khác là optional.

Route type 2 (RT-2) là route quan trọng nhất, trực tiếp điều khiển quá trình kết nối intra-network và inter-network trong EVPN. Tài liệu này sẽ đề cập một cách cụ thể nhất về cấu trúc thông điệp cũng như tác dụng của RT-2 trong từng ca sử dụng cụ thể.

Route type 3 (RT-3) có tác dụng chủ yếu trong việc định tuyến multicast overlay, cũng sẽ được nhắc đến trong phần sau của tài liệu

Route type 5 (RT-5) là một phần mở rộng từ RT-2, giúp EVPN quảng bá IP prefix một cách hiệu quả hơn. Tuy nhiên tài liệu này không đề cập đến tác dụng của loại route này một cách chi tiết.

Bạn đọc có thể tìm hiểu thêm về các loại EVPN route types tại đây: [EVPN Route Types](./route_types.md).

#### Other NLRI information

Bên cạnh RD, có hai thông tin khác được gắn vào trước địa chỉ trong trường NLRI của mỗi bản tin quảng bá route EVPN, bao gồm:
1.  Ethernet Segment ID (ESI): Hỗ trợ EVPN multihoming, sẽ được đề cập trong phần sau của tài liệu này và được phân tích chi tiết trong tài liệu: [EVPN Route Types](./route_types.md)
2.  Ethernet Tag: Dùng trong các loại EVPN VLAN services, được đề cập đến trong tài liệu: [EVPN VLAN services](./vlan_services.md)

Để cho đơn giản, trong tài liệu này, ta coi như ESI bằng 0 (single homing) và Ethernet Tag bằng 0 (vlan-based service) nếu không nói gì thêm.

### Modifications to Support EVPN over eBGP

Để có thể sử dụng eBGP trong EVPN, ta cần thay đổi một vài cấu hình mặc định.

#### Keeping the NEXT HOP Unmodified

Khác với iBGP, eBGP có chuyển tiếp bản tin quảng bá route tới các peers của mình. Khi thực hiện chuyển tiếp một route EVPN, router thay đổi NH trên route thành địa chỉ của mình, làm cho router đích muốn chuyển tiếp traffic tới router nguồn phải điều hướng qua router hiện tại, gây giảm hiệu năng của hệ thống.

Như vậy, cấu hình đầu tiên phải thực hiện khi sử dụng eBGP cho EVPN là disable tính năng tự động cập nhật NH cho route EVPN.

#### Retaining Route Targets

Hành vi mặc định của iBGP routers là drop hết các prefix có route target không được import. Tuy nhiên, đối với EVPN, các spine switches không được phép làm điều này vì chúng có thể phải chuyển tiếp những routes này cho leaves.

Như vậy, cấu hình thứ hai cần thực hiện (trên spines) là lưu trữ tất cả các routes thay vì drop khi RTs trên routes không được import.

## Chapter 4: Bridging with EVPN

Nhiệm vụ chính của EVPN là hỗ trợ kết nối L2 overlay, a.k.a overlay bridging. Chương này sẽ đề cập đến cách thức EVPN xử lý các vấn đề liên quan đến bridging, bao gồm:
-   Control/Data plane semantics associated with bridging
-   Kết nối L2 multicast
-   Xử lý MAC movement và ngăn chặn L2 loops
-   Multihoming
-   ARP suppression

### An Overview of Traditional Bridging

Cơ chế học địa chỉ trong mạng L2 truyền thống là flood-and-learn: Khi thấy một gói tin có địa chỉ MAC không xác định, switch gửi gói tin này qua tất cả các cổng có cùng VLAN với gói tin (flooding), đồng thời lưu địa chỉ nguồn và port mà gói tin gửi đến trong bảng MAC (learning).

Để chống loop trong mạng L2, giao thức STP được sử dụng để tạo ra cây khung mà gói tin sẽ chỉ được forward trên nó.

### Overview of Bridging with EVPN

Xét một môi trường mạng EVPN sử dụng giao thức đóng gói VXLAN chạy trên topo Clos. Hãy nhớ:
1.  Môi trường mạng giữa leaf và spine là môi trường mạng L3 (a.k.a routing), tất cả các link giữa leaf và spine đều có thể được tận dụng để chuyển tiếp gói tin.
2.  VLAN không được cấu hình trên các link kết nối giữa leaf và spine
3.  VTEP được đặt tại leaf
4.  Môi trường mạng giữa leaf và các thiết bị đầu cuối là L2, VLAN được cấu hình trên leaf tại các cổng hướng đến các host.

Trước khi thiết lập kết nối BGP/EVPN, các BGP speakers cần trao đổi các bản tin *BGP Capability Advertisement* chứa cặp AFI/SAFI cho EVPN (l2vpn/evpn), qua đó giúp chúng thống nhất trong việc giao tiếp với nhau thông qua EVPN.

Xét ví dụ môi trường mạng Clos 2 lớp như sau:

![image](/uploads/4ed6b86b044eb7af7e816219938d4fc7/image.png)

Giả sử A và E là hai host cùng thuộc virtual network `red` và địa chỉ MAC của chúng chưa được học bởi router gắn trực tiếp với chúng là L2 và L4. Quá trình gửi gói tin từ A tới E diễn ra như sau:
1.  Các VTEPs trao đổi danh sách virtual networks mà chúng quan tâm đến
1.  A gửi gói tin request tới E
    -   A gửi gói tin tới L1 tại cổng `Port_A`
    -   L1 học được địa chỉ local `MAC_A` tương ứng với cổng vào `Port_A`
    -   L1 không biết route tới E nên nó đóng tunnel VXLAN cho gói tin và gửi tới tất cả các VTEP quan tâm đến virtual network `red` (L2 và L4).
    -   L2 và L4 nhận được gói tin VXLAN gửi đến địa chỉ của mình, chúng bóc tunnel để nhận về gói tin gốc rồi quảng bá nó qua các local ports
    -   E nhận được gói tin mà L4 gửi đến
1.  L1 quảng bá route cho địa chỉ MAC của A
    -   Sau khi L1 học được MAC của A, nó tạo ra một một thông điệp EVPN Route Type 2 (RT-2) mang theo thông tin `{VNI, MAC}` của A. Thông điệp này chỉ ra rằng `MAC_A` thuộc vào virtual network `red` và có thể tiếp cận được (reachable) thông qua VTEP L1.
    -   L1 gửi thông điệp này tới các BGP peers của nó, tại đây có hai trường hợp:
        1.  iBGP: L1 peers với tất cả các leaves (thông qua RR đặt tại S1 và S2), nó chỉ gửi RT-2 tới L2 và L4 do L3 không quan tâm tới virtual network `red`
        2.  eBGP: L1 peers và gửi RT-2 tới S1 và S2. S1 và S2 relay route này tới cả L2, L3 và L4 do spines không biết được leaf nào quan tâm tới network nào
    -   L2 và L4 học được `MAC_A` là địa chỉ remote MAC và có thể đến được thông qua L1
1.  E gửi gói tin response về A
    -   E gán địa chỉ MAC đích cho gói tin response là `MAC_A` và gửi gói tin tới L4 tại `Port_E`
    -   L4 học được địa chỉ local `MAC_E` tương ứng với cổng `Port_E`
    -   L4 do đã học được địa chỉ `MAC_A` nên nó đóng tunnel cho gói tin response đến thẳng L1
    -   L1 nhận được gói tin VXLAN gửi đến mình, bóc tunnel thấy gói tin bên trong có địa chỉ đích là `MAC_A`, nó forward luôn gói tin này qua `Port_A`
    -   A nhận được gói tin response của E
1.  L4 quảng bá route cho địa chỉ MAC của E: L4 quảng bá reachability cho `MAC_E` tương tự như L1 đã làm cho `MAC_A`

Dưới đây là bảng so sánh cơ chế bridging giữa 802.1Q và EVPN

Control protocol                                        | STP                                                                           | BGP EVPN                                                             |
--------------------------------------------------------|-------------------------------------------------------------------------------|----------------------------------------------------------------------|
Knowing which edges are interested in a virtual network | No notion of remote edge; every hop along the way is aware of virtual network | Remote NVE interest in virtual network learned from BGP EVPN message |
Behavior when destination is unknown                    | Flood along spanning tree, only on links carrying that virtual network        | Flood to every VTEP interested in that virtual networka              |
Packet header carrying virtual network of the packet    | 802.1q VLAN tag                                                               | VXLAN header                                                         |
MAC to port association of local end host               | .1Q bridge learning                                                           | .1Q bridge learning                                                  |
MAC to port association of remote end host              | .1Q bridge learning                                                           | BGP EVPN messages                                                    |
All links in network utilized                           | No                                                                            | Yes                                                                  |
Path from A → E versus the path from E → A              | Same path used in forward and reverse paths                                   | Different paths may be used in the forward and reverse paths         |

### What Ifs

Thủ tục bridging thông qua EVPN làm nảy sinh một vài câu hỏi:

> Nếu E response trước khi L4 học được địa chỉ `MAC_A` thì sao?

*Trả lời*: Khi đó L4 lại flood gói tin response trong virtual network `red` tương tự như L1 làm trước đó.

> Nếu gói response của E về L1 trước khi L1 học được `MAC_A` thì sao?

*Trả lời*: Khi đó L1 flood gói tin qua các local ports tương tự như L4 làm với gói tin mà A gửi đến E.

> Trong trường hợp eBGP, L3 sẽ làm gì với RT-2 gửi đến từ L1 và L4?

*Trả lời*: L3 có thể lưu nó lại đề phòng trường hợp sau này có một host thuộc virtual network `red` được gắn với nó, tuy nhiên, common practice là drop nó đi.

### Handling BUM Traffic

BUM (Broad cast, Unknown Unicast & Multicast) traffic trên overlay network cần được xử lý đặc biệt tại underlay. Ví dụ, trong luồng xử lý bridging đề cập đến ở trên, VTEPs cần flood các gói tin chứa địa chỉ MAC đích mà mình chưa biết tới tất cả các VTEPs quan tâm đến virtual network `red`. Ba cơ chế chính thường được triển khai để xử lý BUM traffic là:
1.  Ingress Replication
1.  L3 Multicast in Underlay
1.  Drop BUM traffic

#### Ingress Replication

Cơ chế: Gửi nhiều bản sao của gói tin BUM, mỗi bản sao được đóng trong một tunnel tới một trong số các VTEP quan tâm tới virtual network hiện tại

Nhược điểm:
-   Làm tăng đáng kể tải cho underlay nếu có nhiều BUM packets và có nhiều VTEP đích

Ưu điểm:
-   Dễ triển khai
-   Không làm thay đổi cấu trúc underlay. Underlay network chỉ cần hỗ trợ vận chuyển unicast traffic
-   Không cần cấu hình. Danh sách VTEP đích được tạo ra một cách tự động từ bản tin EVPN Route Type 3 mà không cần đến sự can thiệp của con người
-   Vẫn hoạt động tốt nếu danh sách VTEP đích có ít hơn 128 nodes và số lượng BUM traffic không quá lớn. ARP suppression giúp giảm thiểu đáng kể BUM traffic

#### Underlay Multicast

Cơ chế:
1.  Underlay network phải có cơ chế hỗ trợ multicast routing, thường thông qua họ giao thức Protocol Independent Multicast (PIM)
1.  Mỗi virtual network được map tương ứng với một multicast group và một multicast address
1.  Mỗi multicast group có một multicast tree riêng, cây này span tất cả các VTEPs quan tâm tới virtual network tương ứng
1.  Gói tin BUM gửi từ host nguồn sẽ được đóng tunnel với địa chỉ IP underlay đích là địa chỉ multicast của virtual network tương ứng. Gói tin BUM sẽ được chuyển tiếp trên cây multicast tới tất cả các nút lá là các VTEP đích, đồng thời đảm bảo rằng số lượng replicated message là nhỏ nhất

Ưu điểm:
-   Giảm tối thiểu tải lên underlay network vì gói tin BUM được replicate tại những vị trí tối ưu

Nhược điểm:
-   Phức tạp trong cấu hình và quản lý do cần nhiều giao thức và nhiều tham số cần được cấu hình để xây dựng multicas groups
-   Số lượng multicast group thực tế có thể được quản lý trên underlay nhỏ hơn rất nhiều so với số lượng virtual network => Không thể đảm bảo mapping 1:1 giữa VNI và MG, bắt buộc phải cấu hình nhiều virtual networks cho một multicast group, làm cho VTEPs đích có thể nhận được BUM traffic của virtual networks mà nó không quan tâm

#### Dropping BUM Packets

Ý tưởng: Nếu một địa chỉ nào đó là hợp lệ thì trước sau gì router cũng sẽ học được nó thông qua EVPN. Do vậy thay vì flood & learn một địa chỉ chưa biết, router sẽ drop traffic tới địa chỉ này cho đến khi nó học được route tới nó.

Ví dụ: ARP suppression (sẽ được trình bày ở dưới)

Nhược điểm:
-   Tăng down time
-   Nhiều giao thức overlay có thể hoạt động dựa trên multicast addresses, drop BUM traffic sẽ vô hiệu hóa khả năng này
-   Không thể kết nối tới các silent host: Nếu host A chỉ phản hồi lại request từ host khác mà không chủ động gửi gói tin đi thì (1) VTEP không học được MAC của A nên nó drop tất cả request đến A, (2) Vì A chỉ gửi gói tin khi nhận được request nên do (1), VTEP không bao giờ học được MAC của A, dẫn đến A sẽ không thể kết nối đến từ bên ngoài

Ưu điểm:
-   Dễ dàng thực hiện
-   Không còn BUM traffic trên mạng underlay => Tăng đáng kể hiệu năng
-   Phòng chống các cuộc tấn công từ chối dịch vụ (DDoS)

#### Signaling Which Approach Is Employed

Sử dụng EVPN Route Type 3, các VTEPs có thể thông báo cho nhau cách thức xử lý BUM traffic nào mà chúng hỗ trợ trong trường `Provider Multicast Service Interface`. Các phương thức được gợi ý bởi chuẩn EVPN bao gồm:
-   3: PIM-SSM
-   4: PIM-SM
-   5: PIM-BiDir
-   6: Ingress Replication

Hành vi drop BUM traffic chỉ có thể cấu hình local mà không quảng bá đi trong RT-3 được.

### Handling MAC Moves

Trong môi trường virtual Cloud thường xuyên diễn ra hiện tượng local MAC addresses di chuyển từ VTEP này sang VTEP khác thông qua VM migration. Hầu hết VM khi di chuyển đều gửi đi các bản tin ARPs, các bản tin này là ARP reply nhưng lại không để trả lời cho ARP request nào nên được gọi là Gratuitous ARP (GARP).

Xét trường hợp A di chuyển từ L1 sang L2:
1.  L2 nhận được bản tin GARP mà A gửi đến sau khi migrate và biết được rằng `MAC_A` giờ trở thành địa chỉ MAC local.
1.  L2 cập nhật MAC table để cập nhật đường đi tới `MAC_A` là qua local port thay vì đi qua remote VTEP `L1`
1.  L2 quảng bá local `MAC_A` trong bản tin BGP UPDATE (Type 2) tới `L1` và `L4`. Bản tin này có một trường extended community đặc biệt là `MAC Mobility` với sequence number bằng 0.
1.  L1 nhận thấy có 2 route cho địa chỉ `MAC_A`, một là route local trên L1, một route là local trên L2. Thông thường thì L1 sẽ ưu tiên route mà nó sinh ra, tuy nhiên, do trên route gửi từ L2 có trường `MAC Mobility`, nó biết rằng A đã di chuyển sang `L2` rồi.
1.  L1 cập nhật lại route tới `MAC_A` trỏ sang L2 và thu hồi route cho `MAC_A` trước đó mà nó đã quảng bá đi
1.  Nếu sau đó A lại di chuyển sang VTEP khác, sequence number của trường `MAC Mobility` sẽ tăng lên 1, các VTEPs luôn ưu tiên route có sequence number lớn hơn và cho rằng đó là route tới từ lần migrate gần nhất.

### Support for Dual-Attached Hosts

*Chú ý*: Nội dung mục này trong cuốn sách tại thời điểm dịch đã outdate so với các bản triển khai thực tế. Nội dung bản dịch là sự tổng hợp từ nhiều nguồn tài liệu của các vendor khác nhau

Thông thường trong DC networks, compute nodes được gắn cùng lúc với nhiều hơn một switch. Việc làm này đem lại những lợi ích:
-   Đảm bảo tính dự phòng cho kết nối tới server
-   Thay thế, nâng cấp switch không gây down time
-   Tăng băng thông, cân bằng tải, v.v.

![image](/uploads/6e80531536833fe16025f2caf828797d/image.png)

Tại phía host, để kết nối cùng lúc với hai switches, ta cần cấu hình chập hai links tới switches lại làm một thông qua cơ chế bonding (hay link aggregation) sử dụng giao thức LACP. Host sẽ dùng bonding interface như một logical interface để giao tiếp tới switches, network kernel sẽ lựa chọn forward gói tin qua link nào.

Tuy nhiên, LACP không cho phép các link trong cùng một bond (hay một link aggregation group - LAG) kết nối với nhiều hơn một switch. Thông thường, có hai cơ chế để khắc phục vấn đề này:
1.  Stacking: Hai switches sẽ được stacked lại với nhau (bằng cách nối hai stack port trên chúng) và trở thành một switch duy nhất. Sau đó cấu hình LAG như bình thường trên hai port (ban đầu nằm trên hai switch, giờ coi như một) nối tới hai interface trên bond của host.
1.  EVPN Multihoming: Giải pháp của EVPN giúp một CE có thể cùng lúc gắn vào nhiều PEs khác nhau thông qua kết nối LAG, đồng thời giúp nâng cao hiệu quả của quá trình quảng bá, thu hồi route trong dual-attach model

Cách thứ nhất đơn giản hơn do nó biến dual-attached host thành single-attached và có khả năng hoạt động với EVPN như bình thường. Cách thứ hai phức tạp hơn, sử dụng khái niệm Ethernet Segment (ES) và Ethernet Segment Identifier (ESI) để đại diện cho một LAG, đồng thời sinh ra hai loại route RT-1 và RT-4 để phục vụ quảng bá và thu hồi route hiệu quả trong mô hình multihoming. Cơ chế multihoming được đề cập chi tiết trong tài liệu [này](./route_types.md).

### ARP/ND Suppression
ARP (Address Resolution Protocol) là giao thức học địa chỉ MAC trong mạng IPv4, ND (Neighbor Discovery Protocol) là giao thức tương tự trên mạng IPv6. Thông thường, VTEP sẽ flood các ARP/ND packets cho cả local và remote sites.

ARP/ND Suppression là tính năng trên VTEP hoạt động bằng cách đọc các ánh xạ MAC-IP từ các bản tin ARP rồi lưu trữ vào ARP/ND flood suppression table. Khi nhận được gói tin ARP/ND request tương ứng với một bản ghi trên bảng này, VTEP sẽ trả lời luôn thay vì quảng bá nó tới local và remote sites.

## Chapter 5: Routing in EVPN

### The Case for Routing in EVPN

Bên cạnh khả năng briding cho traffic trong cùng một virtual network, VTEPs cần có khả năng routing cho traffic giữa các virtual network. IETF đã đề xuất một bản phác thảo có tên "[draft on integrated routing and bridging](https://tools.ietf.org/html/draft-ietf-bess-evpn-inter-subnet-forwarding-10)" để  hỗ trợ khả năng này.

Nội dung chương này tập trung nghiên cứu khả năng định tuyến cho virtual networks trong EVPN.

### Routing Models

Routing Overview:
-   Router có một cổng (interface) tương ứng với mỗi L2 network gắn với nó, gọi là Switched VLAN Interface (SVI)
-   Mỗi SVI có một địa chỉ IP và ít nhất một địa chỉ MAC. Địa chỉ IP của SVI là *default gateway* trên các host nằm trên L2 network tương ứng. Nếu host không xác định được địa chỉ đích cho gói tin, nó sẽ chuyển tiếp gói tin tới địa chỉ IP này.
-   Khi router nhận được gói tin trên một SVI, nó sẽ thực hiện định tuyến bằng cách:
    1.  Tìm kiếm trên bảng chuyển tiếp IP (a.k.a Forwarding Information Base (FIB)) theo key là Virtual Routing Forwarding (VRF) và IP đích của gói tin, thu về thông tin cổng ra và next hop (NH) tương ứng.
    1.  Router lấy về địa chỉ MAC của NH bằng cách: (1) Nếu NH là đích thì `MAC = NH->MAC`, (2) Nếu NH là nút trung gian thì tra bảng ARP lấy về MAC tương ứng với địa chỉ IP của NH
    1.  Nếu cổng ra là một SVI, router tìm kiếm địa chỉ MAC của NH trên bảng MAC của L2 network tương ứng, thu về cổng ra thực tế tới NH. Router forward gói tin qua cổng ra tới NH.

Các câu hỏi được đặt ra khi định tuyến với EVPN là:
1.  Đặt gateway tại vị trí leaf nào, tại một leaf hay tất cả các leaf? Đánh địa chỉ gateway như thế nào, dùng chung IP hay dùng IP khác nhau cho các gateway trong cùng một network?
1.  VXLAN cung cấp L2 overlay network, làm thế nào để chuyển tiếp routed packet? Trường VNI trên gói tin underlay giữa hai network là của network nguồn hay network đích? Địa chỉ đích của gói tin overlay là địa chỉ của host đích hay gateway đích? Nếu VTEP không chứa network đích, làm thế nào để nó chuyển tiếp gói tin đến đúng VTEP đích?

Có nhiều mô hình được thiết kế để trả lời những câu hỏi này, trong đó phân chia thành hai xu hướng chủ đạo: tập trung vs phân tán, đối xứng vs bất đối xứng.
1.  Định tuyến tập trung: Gateway cho mỗi virtual network chỉ được đặt tại một leaf
1.  Định tuyến phân tán: Mỗi leaf đóng vai trò gateway cho tất cả virtual networks mà nó quản lý
1.  Mô hình đối xứng: Sau định tuyến, gói tin được chuyển tiếp giữa VTEP nguồn và VTEP đích sử dụng một VNI tag khác với cả network nguồn và đích
1.  Mô hình bất đối xứng: Sau định tuyến, gói tin được VTEP nguồn chuyển tiếp tới host đích sử dụng VNI của network đích

Để đảm bảo tính tương thích giữa nhiều hệ thống, dung hòa xung đột giữa quan điểm thiết kế của các vendors và để phù hợp với nhu cầu thực tiễn, ta cần hiểu được đặc trưng của tất cả những mô hình này.

### Where Is the Routing Performed?

#### Centralized Routing

Đây là mô hình đơn giản nhất, trong đó sử dụng cặp border leaf làm gateway cho các virtual networks:
![image](/uploads/06c37442595db1edf81ab88f209ffe87/image.png)

Ưu điểm:
-   Duy trì được mô hình first-hop routing vốn có
-   Phù hợp với north-south traffic
-   Có thể triển khai các dịch vụ firewall, LB trên các router này
-   Dễ dàng tích hợp dần các leaf switch có hỗ trợ VXLAN vào môi trường mạng cũ mà không phải nâng cấp hàng loạt các thiết bị mạng

Nhược điểm:
-   Khả năng mở rộng kém do nhiệm vụ định tuyến đặt lên một vị trí duy nhất
-   Đường đi của traffic không tối ưu do traffic giữa hai network phải đi qua border leaf
-   Không phù hợp với hạ tầng có nhiều east-west traffic (tăng trễ và khả năng tắc nghẽn)
-   Khi host di chuyển sang VTEP khác, NH trên đường đi từ host tới gateway thay đổi nên phải học lại

Trong trường hợp cặp border leaf hoạt động theo mô hình active-active, ba giải pháp có thể triển khai để tránh tình trạng duplicate gói tin là:
1.  Sử dụng VTEP anycast IP address và ingress replication cho BUM traffic (recommended vì tính đơn giản)
1.  Sử dụng VRRP
1.  Sử dụng designated forwarder như L2 multihoming

#### Distributed Routing

Trong mô hình distributed routing, tất cả các VTEP đều đóng vai trò gateway cho virtual networks mà chúng quản lý. Bên cạnh đó, SVI của cùng một virtual network trên tất cả các VTEPs đều có chung địa chỉ IP gateway và MAC.

Ưu điểm:
-   Dễ dàng mở rộng do thiết kế nhất quán cũng như khả năng cân bằng tải cao
-   Đường đi cho traffic luôn là tối ưu, phù hợp với east-west traffic
-   Đơn giản, không đòi hỏi các giao thức khác đi kèm
-   MAC Mobility: Dễ dàng chuyển host từ VTEP này qua VTEP khác mà không cần học lại địa chỉ gateway (do gateway gắn trực tiếp với host với MAC và IP như cũ).

Nhược điểm:
-   Khó triển khai network services (firewall, lb, ...). North-south traffic vẫn có thể đi qua services trên border leaf switches như thông thường. Tuy nhiên đối với east-west traffic thì cần triển khai một trong hai giải pháp: (1) Triển khai dịch vụ tại host và (2) Sử dụng VRF để isolate traffic và điều hướng qua border gateway đang chạy services

Có thể triển khai cả distributed routing (cho east-west traffic) và centrailized routing (cho north-south traffic) trong cùng một data center.

### How Routing Works in EVPN?

Hai giải pháp định tuyến thường dùng trong EVPN là định tuyến bất đối xứng (asymmetric routing) và định tuyến đối xứng (symmetric routing).

#### Asymmetric Routing

Trong định tuyến bất đối xứng, hành vi của VTEP nguồn và đích là khác nhau:
-   VTEP nguồn tra cứu route để xác định VTEP đích, đồng thời đóng tunnel cho gói tin overlay với VNI của network đích (routing)
-   VTEP đích nhận được gói tin underlay chỉ thực hiện bóc tunnel và chuyển tiếp tới host đích (bridging)

Sau khi VTEP nguồn biết cần route packet tới VTEP đích và set VNI của virtual network đích cho gói tin VXLAN thì quá trình kết nối giữa host nguồn và host đích diễn ra giống hệt như bridging.

Tuy nhiên, vấn đề của định tuyến bất đối xứng là nếu như VTEP nguồn không biết về network đích, nó sẽ không nhận được route EVPN từ VTEP đích gửi về và không biết cách để tiếp cận IP đích thông qua VTEP nào. Vấn đề này được giải quyết bởi định tuyến đối xứng, sẽ được trình bày sau đây.

#### Symetric Routing

Trong định tuyến đối xứng, cả VTEP nguồn và đích đều thực hiện nhiệm vụ routing, cụ thể:
-   VTEP nguồn đóng tunnel và định tuyến gói tin underlay tới địa chỉ VTEP đích
-   VTEP đích bóc tunnel định tuyến gói tin tới địa chỉ đích cuối cùng
-   VNI được gắn trên VXLAN packet là một L3 VNI, khác với L2 VNI của network nguồn và network đích

Điểm đặc biệt của symmetric routing là nó sử dụng thêm một bảng L3 VRF (hay IP-VRF) thực hiện nhiệm vụ định tuyến giữa các virtual networks. Các đặc điểm của VRF này bao gồm:
-   Lưu trữ mapping từ prefix là địa chỉ IP của các virtual networks tới NH là địa chỉ VTEP nguồn tương ứng
-   Được gán một VNI riêng, gọi là một L3 VNI (đối lập với L2 VNI dùng bên trong các virtual networks)
-   Các virtual networks cần lưu trữ tham chiếu tới IP-VRF để có thể được định tuyến tới virtual networks khác thông qua cơ chế import/export IP prefix

Thủ tục xây dựng bảng IP-VRF trong EVPN diễn ra như sau:
-   VTEP nguồn quảng bá RT-2 cho các địa chỉ MAC mà nó học được như thông thường
-   Trong các bản tin RT-2, bên cạnh thông tin địa chỉ MAC, L2 VNI của virtual network và RT tương ứng với L2 VNI, VTEP còn có thể đính kèm các thông tin liên quan đến địa chỉ IP bao gồm:
    -   Địa chỉ IP gắn với MAC
    -   L3 VNI lấy từ cấu hình IP-VRF mà virtual network tham chiếu tới
    -   L3 RT tương ứng với L3 VNI
-   VTEP đích bên cạnh việc cập nhật bảng định tuyến L2 (hay MAC-VRF) cho virtual network, nó còn cập nhật IP-VRF từ những thông tin liên quan đến địa chỉ IP đính kèm trên cùng bản tin RT-2 như sau:
    -   Lấy về IP-VRF tương ứng với L3 VNI
    -   Đối chiếu import RT của IP-VRF với L3 RT trên bản tin RT-2, nếu khớp thì tiến hành import với prefix là địa chỉ IP và NH là địa chỉ của VTEP đích

Với việc sử dụng thêm các trường của RT-2 như trên, VTEP nguồn đã có thể định tuyến gói tin underlay tới VTEP đích sử dụng IP-VRF. Việc cần làm tiếp theo là khiến VTEP đích thực hiện định tuyến cho gói tin overlay sau khi bóc tunnel, bằng cách gán lại địa chỉ MAC của gói tin overlay thành địa chỉ MAC của VTEP đích. Trước đó, địa chỉ MAC của VTEP đích cũng đã được quảng bá trong bản tin RT-2 trong một extended community tên là *Router's MAC Extended Community*. Khuôn dạng đầy đủ của một bản tin RT-2 trong trường hợp định tuyến đối xứng như sau:

```
+------+---------------------------------------+
|      |  RD (8 octets)                        |
|      +---------------------------------------+
|      |  ESI (10 octets)                      |
|      +---------------------------------------+
|      |  Ethernet Tag ID (4 octets)           |
|      +---------------------------------------+
|      |  MAC Address Length (1 octet)         |
|      +---------------------------------------+
| NLRI |  MAC Address (6 octets)               |
|      +---------------------------------------+
|      |  IP Address Length (1 octet)        * |
|      +---------------------------------------+
|      |  IP Address (0, 4, or 16 octets)    * |
|      +---------------------------------------+
|      |  MPLS Label1 (3 octets) or L2 VNI     |
|      +---------------------------------------+
|      |  MPLS Label2 (3 octets) or L3 VNI   * |
+----------------------------------------------+
|      |  MAC-VRF (L2) Route Target            |
|      +---------------------------------------+
| Ext- |  IP-VRF (L3) Route Target           * |
| Comm +---------------------------------------+
|      |  Router's MAC                       * |
|      +---------------------------------------+
|      |  Tunnel Encapsulation (VXLAN)         |
+------+---------------------------------------+
|                   Next Hop                   |
+----------------------------------------------+

(*): Các trường được sử dụng thêm bởi symmetric routing
```

Giờ ta sẽ tìm hiểu quá trình định tuyến đối xứng cho gói tin gửi từ A đến F trong hình vễ dưới đây:

![image](/uploads/cf506a2e0a691a8ba05c9248565dc3b1/image.png)

Các bước định tuyến gói tin diễn ra như sau:
1.  Trên các VTEPs đều được cấu hình `green` IP-VRF giúp định tuyến gói tin giữa `red` và `blue` network
2.  A biết F khác network với mình nên gửi gói tin tới default gateway đặt tại L1
3.  L1 nhận, đóng tunnel cho gói tin và chuyển tiếp tới L4
    -   L1 thực hiện định tuyến gói tin do địa chỉ MAC đích là MAC của nó
    -   L1 thấy 2 routes có thể đi tới địa chỉ MAC đích: (1) Route tới L4 trên `green` IP-VRF được quảng bá trước đó trong thông điệp RT-2 và (2) Route tới prefix 10.1.2.0/24 tương ứng với local SVI gắn với `blue` network. Do route (1) có netmask lớn hơn (/32) nên nó được chọn
    -   L1 đóng tunnel cho gói tin tới L4 theo các bước: (1) Thay MAC nguồn của gói overlay là MAC của chính nó, (2) Thay MAC đích của gói overlay thành MAC của L4, (3) Thêm VXLAN header với VNI của `green` network, (4) Thêm UDP header và (5) Thêm outer Ethernet header
4.  L4 nhận, mở tunnel và thực hiện định tuyến cho gói tin overlay
    -   L4 bóc tunnel, phát hiện địa chỉ MAC của gói overlay trùng với địa chỉ MAC của nó nên thực hiện định tuyến cho gói tin này
    -   L4 tìm kiếm địa chỉ IP 10.1.2.F trên `green` VRF, phát hiện địa chỉ này là địa chỉ local trên `blue` network
    -   L4 tìm kiếm địa chỉ IP 10.1.2.F trên bảng ARP của `blue` network, phát hiện địa chỉ MAC tương ứng là `MAC_F` và cổng ra `Port_F`, bèn forward gói tin tới F qua cổng này

Nếu ta gửi gói tin từ `C` tới `F`, định tuyến đối xứng vẫn có thể diễn ra bình thường do quá trình định tuyến tại VTEP nguồn (`L2`) không cần dùng đến thông tin nào liên quan đến network `blue`. Chú ý, dù `L2` không gắn với network `blue` nhưng do nó có cấu hình `green` IP-VRF, RT-2 từ network blue vẫn được quảng bá tới nó. Khi đó, `L2` sẽ chỉ import IP prefixes vào IP-VRF mà không import MAC prefix vào bảng MAC-VRF.

Có thể thấy trong mô hình định tuyến đối xứng, hành vi của router đích bất kỳ là hoàn toàn tương tự như một border gateway. Nói cách khác, định tuyến đối xứng bắt buộc phải được triển khai theo mô hình phân tán.

#### Comparison between symmetric and asymmetric routing

-   So sánh cách đóng gói tunnel tại source VTEP:
    ![How the fields in the VXLAN packet are populated by L1 on routing IPF](./images/evpn_0506.png)
-   Khả năng mở rộng: Asymmetric routing đòi hỏi remote network cũng phải tồn tại trên local, làm hạn chế đi khả năng mở rộng của số lượng kết nối trong môi trường EVPN
-   Khả năng tương thích: Asymmetric routing dễ dàng tương thích với mô hình tập trung truyền thống, trong khi symmetric routing bắt buộc mô hình định tuyến phải là phân tán
-   Cấu hình: Symmetric model đòi hỏi cấu hình thêm IP VRF trên các VTEPs
-   Sử dụng VRFs: Asymmetric model có thể dùng chung default VRF, trong khi symmetric model yêu cầu một VRF riêng biệt để cấu hình L3 VRF

### Configuring and Administering EVPN

Trong phần này, tác giả hướng dẫn cách cấu hình EVPN trong FRR, bạn đọc có thể tự tìm hiểu thêm.

### Considerations for Deploying EVPN in Large Networks

Ngoại trừ RT-5, tất cả các route EVPN đều chứa prefix /32. Với mô hình Clos 2 lớp, khi số lượng prefix kiểu này vượt quá 120000 routes và số lượng leaves vượt quá 128 nút, EVPN/VXLAN bắt đầu đối mặt với những vấn đề về khả năng mở rộng, trong đó có thể kể đến như:
-   Kích thước bảng chuyển tiếp trở nên quá lớn
-   Số lượng VNIs quá nhiều
-   Ingress replication list có kích thước lớn
-   v.v.

Trong môi trường DC lớn, giải pháp đề ra là sử dụng mạng Clos 3 lớp thay cho mạng Clos 2 lớp thông thường:

![image](/uploads/1960f4b7d740a1879d823c0b3395fd4c/image.png)


Trong môi trường này, mỗi pod nên được phân hoạch một số lượng VNI nhất định, hạn chế tối đa việc span một VNI ra nhiều pod. Nhiệm vụ kết nối các pod nên được đặt tại các exit leaf thay vì các spine, giúp tổng hợp prefix trong pod và quảng bá cũng như nhận route thông qua các inter-pod spines.

## References

-   [E-book "EVPN in the Datacenter"](https://learning.oreilly.com/library/view/evpn-in-the/9781492029045/)
-   [RFC 4760: Multiprotocol Extensions for BGP-4](https://tools.ietf.org/html/rfc4760)
-   [RFC 7432: MPLS-Based EVPN](https://tools.ietf.org/html/rfc7432)
-   [RFC 8365: EVPN in adaptation  in VXLAN networks](https://tools.ietf.org/html/rfc8365)
-   [RFC draft: Integrated Routing and Bridging in EVPN](https://tools.ietf.org/html/draft-sajassi-l2vpn-evpn-inter-subnet-forwarding-05)
-   [Arista: IRB Concepts](https://www.arista.com/en/um-eos/eos-section-23-3-integrated-routing-and-bridging)
-   [Juniper: EVPN implementation white paper](https://www.juniper.net/assets/us/en/local/pdf/whitepapers/2000606-en.pdf)
