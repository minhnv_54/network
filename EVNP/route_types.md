## EVPN Route Types
> Tài liệu này đề cập đến các loại EVPN route types, bao gồm cấu trúc thông điệp và phạm vi sử dụng của chúng.

### Table of Contents

* [EVPN Route Types](#evpn-route-types)
  * [Table of Contents](#table-of-contents)
  * [Overview](#overview)
  * [Type 1: Ethernet Auto-Discovery (AD) route](#type-1:-ethernet-auto-discovery-(ad)-route)
  * [Type 2: MAC/IP Adertisement.](#type-2:-mac/ip-adertisement.)
    * [Fast Convergence](#fast-convergence)
  * [Type 3: Inclusive multicast Ethernet tag route](#type-3:-inclusive-multicast-ethernet-tag-route)
    * [1. Underlay Multicast](#1.-underlay-multicast)
    * [2. Ingress Replication.](#2.-ingress-replication.)
  * [Type 4: Ethernet Segment Route](#type-4:-ethernet-segment-route)
  * [Type 5: IP Prefix Advertisement](#type-5:-ip-prefix-advertisement)
  * [References](#references)

### Overview

EVPN sử dụng trường `Type` trong NLRI của route để xác định thông tin prefix mà route quảng bá đi là gì. Dưới đây là danh sách các EVPN Route Types chính và tác dụng của chúng:

Route Type | What it carries                 | Primary use                                                                                   |
-----------|---------------------------------|-----------------------------------------------------------------------------------------------|
Type 1     | Ethernet Segment Auto Discovery | Used in the data center in support of multihomed endpoints                                    |
Type 2     | MAC, VNI, IP                    | Advertises reachability to a specific MAC address, and optionally its IP address              |
Type 3     | VNI/NVE Association             | Advertises reachability in a virtual network                                                  |
Type 4     | Multicast Information           | Supports multihomed endpoints, ensures that only one of the NVEs forwards multicast packets   |
Type 5     | IP Prefix, L3 VNI               | Advertises prefix (not /32 or /128), routes such as summarized routes in a virtual L3 network |

Trong số các loại route này, RT-2 là route quan trọng nhất, phục vụ cả bridging trong một overlay L2 network và routing giữa các L2 networks với nhau. Tài liệu [EVPN in Data Center](./evpn_in_dc.md) đã trình bày rất rõ về format và cách thức sử dụng RT-2 trong bridging và routing trong EVPN. Tài liệu này tập trung vào phân tích chức năng của các loại route còn lại.

### Type 1: Ethernet Auto-Discovery (AD) route

Yêu cầu trong các data center là việc dự phòng các top-of-rack devices (server multihoming). Trong các traditional network, việc này phụ thuộc vào các vendor khác nhau sẽ sử dụng một giải pháp khác nhau như MLAG, MC-LAG, VCP,... Tất nhiên các giải pháp này phụ thuộc vendor và các có một số vấn đề.

EVPN nói 1 cách khác là một giải pháp multihoming chuẩn (standards-based multihoming), có thể scale với số lượng PE tùy ý và giải quyết vấn đề vendor lockin.

Route Type 1 được sử dụng cho EVPN server multihoming:

![image](/uploads/14701e33bbcce57214b6f3bed6249b5a/image.png)

Khái niệm Ethernet Segment được dùng để dại diện cho CE và các PEs gắn với nó trong một liên kết multihoming. Ethernet Segment Identifer được dùng giúp một PE xác định các PEs nào đang cùng kết nối tới CE gắn với nó. Nếu CE là single homed thì ESI bằng 0.

Trong trường hợp này H2, đang được kết nối trực tiếp đến LS2 và LS3. Do đó, cả LS2 và LS3 đều quảng bá khả năng direct reachable đến ESI của H2. Việc quảng bá này sẽ thông qua Route Type 1. Thông tin của Route type 1 sẽ có:
- ESI
- Loopback IP của LS2 (PE gửi route)

Như vậy là LS1 sẽ nhận được H2 sẽ có thể được reach thông qua cả LS2 và LS3. Sau đó, khi 1 CE dưới LS1 muốn reach đến H2 thì LS1 sẽ sử dụng LoadBalance thông ECMP.

**Kết luận:** Route Type 1 sẽ quảng bá ESI đang được attach đến một PE và sẽ quảng bá đến tất cả các MP-BGP peers.

### Type 2: MAC/IP Adertisement.

Đối với thông tin MAC/IP của một CE, BGP EVPN sẽ quảng bá thông qua route type 2. Theo đó, một PE sẽ quảng bá MAC/IP route của tất cả các CE của nó đến tất cả các MP-BGP peers.

![image](/uploads/40bf67e84f20d542d11737bf0d1ad0ff/image.png)

Như hình trên, LS1 sẽ nhận được một gói tin route type 2 cho địa chỉ MAC của H2 từ LS3 với ESI được liên kết là ESI `0:1:1:1:1:1:1:1:1:1`. Tương tự LS1 cũng sẽ nhận được route type 2 cho MAC của H2 từ LS2 --> H2 có thể được reach thông qua LS2 và LS3.

**Vấn đề**: Trường hợp 2 route cùng 1 MAC sẽ xuất hiện trong bảng FIB của LS1. LS1 sẽ sử dụng ECMP để loadbalance traffic.

![image](/uploads/042e436298d5e45595e42192a32639f9/image.png)

**NOTE**: Do LS1 nhận được rằng H2 được reach thông qua LS2 và liên kết với ESI `0:1:1:1:1:1:1:1:1:1`. Sau đó, LS1 cũng sẽ nhận biết được rằng ESI này cũng được reach từ LS3. Do đó, H2 cũng sẽ được reach thông qua LS3.

#### Fast Convergence

Việc hội tụ sau khi một Lỗi trên network sẽ được xử lý như sau:
- Connection giữa LS3 và switch S bị down
- LS3 sẽ gửi các bản tin withdraw routes đối với tất cả các host dưới switch S.

![image](/uploads/6564f48b53ee411bec62b67c9f59f4a8/image.png)


Như vậy, nếu như dưới switch S có 100 host thì LS3 phải gửi 100 withdraw route đến LS1. Cho đến khi LS1 nhận được hết 100 withdraw route thì LS1 vẫn tiếp tục gửi traffic đến các host này tới LS3.

EVPN xử lý như sau:
- Khi connection giữa LS3 đến switch S bị down.
- LS3 sẽ gửi các gói tin withdraw đối với ESI routes trước khi gửi các gói tin withdraw host routes.
- Khi LS1 thấy ESI bị withdraw thì nó sẽ remove tất cả route trên ESI trong FIB.

![image](/uploads/52dcd167833fbf6ff4e82ed902e312c5/image.png)


Như vậy, việc hội tụ sẽ rất nhanh vì LS3 sẽ withdraw route type 1 trước khi withdraw route type 2.

### Type 3: Inclusive multicast Ethernet tag route

Có một sự nhầm lẫn răng là VXLAN với BGP EVPN không sử dụng multidestination traffic. Trong một số trường hợp thì BUM traffic vẫn cần được xử lý như đối với các silent host, hay DHCP,...

VXLAN với BGP EVPN có 2 cách xử lý BUM traffic:
- Tận dụng multicast replication trong underlay.
- Sử dụng giải pháp mulitcast-less hay còn gọi là ingress replication. Cơ bản là sử dụng multiple unicast stream để forward multidestination traffic đến các recipient tương ứng.

#### 1. Underlay Multicast
Giải pháp này tận tính năng multicast group trong underlay. Khi khởi tạo VTEP, nó sẽ join vào multicast group tương ứng. Khi một host dưới VTEP đó gửi 1 gói tin broadcast. Local VTEP sẽ broadcast gói ARP đến các VTEP trong cùng group.

#### 2. Ingress Replication.
Ingress Replication hoặc Head-end Replication (HER) là một giải pháp dựa trên unicast traffic. Tức là Ingress hoặc source VTEP sẽ tạo ra n-1 bản copy BUM packet và gửi bản copy này như unicast traffic đến N-1 VTEP tương ứng (VTEP có join vào VNI membership). Trong IR/HER thì danh sách các replication được tạo ra staticlly hoặc dynamic thông qua BGP EVPN control plane (Thông qua route-type 3 - Inclusive multicast Ethernet tag - IMET).
--> Để đạt được hiệu qủa cao nhất thì BGP EVPN được sử dụng để dynamic xây dựng danh sách replication. Dach sách replication này chứa egress/destination VTEP mà join vào cùng một L2 VNI.

**Cơ chế**: Mỗi VTEP sẽ advitise một route chứa (VNI và Next-hop IP là địa chỉ của chính VTEP đó) --> Dynamic replication list sẽ được build. List này sẽ được cập nhật khi một VNI config trên VTEP xuất hiện. Việc tạo ra multiple replication được thực hiện trên VTEP.

**BUM traffic được xử lý như sau:**
Nếu PE nhận 1 BUM packet từ 1 local CE device:
- Flood đến local CE trong cùng bridge domain
- Flood đến remote PE device trong cùng bridge domain
- Không flood đến source CE của BUM packet.

Nếu PE nhận BUM packe từ một remote PE device:
- Flood đến local CE device trong cùng bridge domain
- Không flood đến các remote PE khác.

![image](/uploads/f0b0db62b8e0076b6a36752f96725d91/image.png)

**Vấn đề:** Trong trường hợp multihoming, khi một CE có thể sẽ nhận duplicate packets từ các PE mà CE đó attach đến.

![image](/uploads/2029bb97a5d96a89ace4068b05d06016/image.png)

Để giải quyết vấn đề này thì EVPN có giới thiệu khái niệm **designated forwarder(DF)**, được sử dụng để loadbalance giữa các DF và chống lại duplicate packet được gửi từ multihoming.

### Type 4: Ethernet Segment Route
Designated forwarder được chọn cho các ESI dựa trên route type 4.

![image](/uploads/311dde3741f1b60876510bdd8d32843c/image.png)

Khi một PE biết được 1 ESI được attach đến nó. PE đó sẽ quảng bá một ES route đến các MP-BGP peers. Sau đó, nó sẽ start một timer (default là 3s) để cho phép nhận các ES route từ các PE khác mà được kết nối đến cùng ES. Timer nên được cấu hình giống nhau trên tất cả các PE cùng ES.

Sau khi timer expire, mỗi PE sẽ xây dựng được 1 danh sách có thứ tự bao gồm địa chỉ của các PEs cùng  kết nối đến ES (bao gồm chính nó). VLANs sẽ được chia đều cho các PEs, mỗi PE chịu trách nhiệm xử lý gói tin của một số VLANs theo một quy tắc chung (VD: PE thứ nhất xử lý các VID lẻ, PE thứ hai xử lý các VID chẵn). Do danh sách PEs là giống nhau và quy tắc phân chia VLAN là thống nhất nên các PEs đạt được đồng thuận trong việc PE nào chịu trách nhiệm xử lý gói tin trên những VLANs nào.

Khi đã có DF, trường hợp duplicate packet sẽ được xử lý. Cụ thể là các gói BUM đến PE có multihomed host sẽ nhận biết nó có phải là DF cho VLAN đó không. Nếu đúng thì nó sẽ forward gói tin xuống CE, ngược lại sẽ drop.



### Type 5: IP Prefix Advertisement

Trước khi đọc nội dung mục này, bạn đọc vui lòng đọc trước nội dung chương "Routing in EPVN" trong tài liệu [EVPN in Data Center](./evpn_in_dc.md) để tìm hiểu cách EVPN định tuyến giữa các virtual networks sử dụng RT-2.

Các vấn đề với EVPN routing sử dụng RT-2:
-   IP prefix bắt buộc phải có mask là /32, định tuyến dùng aggregated IP prefix là không khả thi
-   MAC và IP luôn phải đi kèm với nhau, không thể tách rời việc định tuyến dựa trên địa chỉ IP khỏi địa chỉ MAC

Một trong những ví dụ cho sự hạn chế của RT-2 trong routing là floating IP. Giả sử địa chỉ floating IP `FIP` lúc đầu gắn với địa chỉ MAC `M1`, NVE thực hiện quảng bá 1000 routes cho địa chỉ `FIP` trong các bản tin RT-2 cùng với địa chỉ MAC tương ứng `M1`. Sau khi `FIP` chuyển sang gắn vào MAC `M2`, NVE lại phải thu hồi hết 1000 RT-2 cho `FIP` với MAC `M1` và quảng bá 1000 RT-2 cho `FIP` với MAC `M2`. Đây rõ ràng là một nhược điểm lớn.

RT-5 giúp giải quyết vấn đề này như sau: Khi học được một địa chỉ `MAC_A/IP_A`, bên cạnh việc gửi RT-2 cho cặp địa chỉ này tới các CE cùng network, CE còn gửi các bản tin RT-5 để quảng bá prefix `IP_A/32` tới cả các CE khác networks (mà được cấu hình chung một IP-VRF).

Khi floating IP thay đổi MAC (nhưng không thay đổi CE), thì CE đó chỉ việc thông báo cập nhật lại địa chỉ MAC cho IP này tới các CE khác trong cùng network, mà không phải thông báo tới các CE khác network do route tới IP prefix không đổi. Và vì floating IP thường được truy cập từ clients nằm trên network khác, có thể tiết kiệm được một số lượng lớn RT-2 không phải thu hồi do sử dụng RT-5.

Từ những nhu cầu trên, RT-5 được đề xuất để phục vụ quảng bá IP prefix. Khuôn dạng của bản tin này như sau:
```
    +---------------------------------------+
    |      RD   (8 octets)                  |
    +---------------------------------------+
    |Ethernet Segment Identifier (10 octets)|
    +---------------------------------------+
    |  Ethernet Tag ID (4 octets)           |
    +---------------------------------------+
    |  IP Prefix Length (1 octet, 0 to 32)  |
    +---------------------------------------+
    |  IP Prefix (4 octets)                 |
    +---------------------------------------+
    |  GW IP Address (4 octets)             |
    +---------------------------------------+
    |  MPLS Label (3 octets)                |
    +---------------------------------------+
```

Trong đó:
-   RD, ESI, ETI tương tự như RT-2
-   IPL/IPP: Địa chỉ IP prefix
-   GW IP: Nếu NH là gateway thì giá trị này bằng IP của gateway, ngược lại thì bằng 0
-   MPLS Label: Phục vụ phân giải địa chỉ để tìm ra NVE đích

### References
- [EVPN Route Type 5](https://tools.ietf.org/html/draft-ietf-bess-evpn-prefix-advertisement-11)
