# EVPN VLAN Service Interfaces

  

> Tài liệu này đề cập tới các loại evpn vlan services, tác dụng cũng như khả năng hỗ trợ và tương thích giữa các vendors

  

## I. Overview

  

EVPN là công nghệ dùng để tạo ra một overlay virtual network trên hạ tầng mạng IP/MPLS hoặc IP/EVPN. Mỗi EVPN instance (EVI) đại diện cho một virtual network mà có thể span ra nhiều PE devices khác nhau. Trên từng PE device, mỗi EVI sở hữu một tập hợp các Route Targets để import/export EVPN routes và một bảng MAC-VRF để lưu trữ những routes này.

  

Mỗi virtual network lại có thể được chia nhỏ thành các broadcast domains khác nhau, mỗi broadcast domain là một VLAN, định danh bởi một hoặc nhiều VLAN ID (VID). BUM traffic gửi tới PEs sẽ chỉ được flood đi bên trong một VLAN tương ứng. EVPN VLAN service interfaces là các giải pháp giúp chuyển tiếp VLAN tagged traffic bên trong một virtual network giữa các PE devices.

  

Ta có thể đánh tag VLAN một cách rất linh hoạt:

- Các EVI's có thể dùng chung VID để định danh broadcast domains

- VID của cùng một VLAN trên các PE khác nhau có thể khác nhau

  
![image](/uploads/267173d2c8042f8f364d46622b0dc246/image.png)


  

Như vậy, để có chuyển tiếp traffic trong từng VLAN, PE thực hiện điều này thông qua:

- EVPN Label: Mỗi PE lưu trữ các ánh xạ giữa VID và EVPN label tương ứng. Sau khi unpack gói tin, PE đích chèn thêm VLAN tag chứa VID cho ethernet frame dựa trên ánh xạ này. Trong trường hợp sử dụng VID khác nhau, PE ghi đè VID nguồn gửi sang bằng VID đích. Đây chính là cơ chế dịch VID (dịch VID này sang VID khác) mà ta sẽ nói đến nhiều trong tài liệu này.

- Ethernet Tag: Là một trường trong prefix của EVPN RT-2, đại diện cho broadcast domain mà địa MAC trong prefix của gói tin thuộc về. Ethernet tag ID có kích thước 32 bit chứa 12 bit biểu diễn VID trong mạng MPLS hoặc 24 bit biểu diễn VNI trong mạng VXLAN.

- Overlay VID: Dùng luôn VID của VLAN trên PE nguồn mà không thông qua cơ chế dịch

  

Có ba loại VLAN service interface chính, bao gồm: VLAN-Based, VLAN-Bundle và VLAN-Aware Bundle service. Trước hết, hãy tìm hiểu các đặc điểm cơ bản của từng loại.

 
##  II. VLAN service interface
### 1. VLAN-Based Service Interface

  

Đặc điểm:

- Một EVI chỉ chứa một VLAN (overlay) và một Label (underlay) tương ứng, tạo ra ánh xạ 1:1 giữa VLAN và EVPN Label trên từng PE

- MAC-VRF của EVI chứa một bridge table tương ứng với VLAN đó

- Nếu VID của VLAN là khác nhau trên các PE khác nhau thì: (1) PE nguồn NÊN giữ nguyên VID trên overlay ethernet frame, (2) PE đích ghi đè VID gốc bằng cách sử dụng mapping VID:Label mà nó có (dịch VID trên PE đích).

- Ethernet Tag ID không được sử dụng (gán bằng 0)
  

Minh họa:
  

![image](/uploads/e8ff925d1472020768be37303e242021/image.png)

  

Ưu điểm:

- Hỗ trợ dịch VID

- Flooding hiệu quả: Gói tin broadcast trên overlay VLAN tại PE nguồn chỉ được gửi đến PE đích chứa VLAN đó (do mỗi Label chỉ mang theo một VID)

  

Nhược điểm:

- Khả năng mở rộng kém: Với mỗi một VLAN cần phải cấu hình một EVI, tương ứng với một MAC-VRF table và một RT set

#### Flooding BUM traffic in VLAN-Based
  
Trong trường hợp này, do mỗi VRF tương ứng với một VLAN, PE chỉ cần flood traffic dựa trên Label là có thể đảm bảo traffic đến được VLAN đích mong muốn.


![image](/uploads/4d452719c88fb67eea9e3756ce15579b/image.png)


Luồng flooding traffic diễn ra như sau:

1. Host trên VLAN-y gửi BUM traffic

2. PE1 nhận được traffic trên VLAN-y

3. PE1 xác định các PE cần flood traffic đến dựa trên RT-y và label L-y, trong trường hợp này là PE2

4. PE1 flood traffic cho PE2

5. PE2 nhận traffic từ PE1

6. PE2 đánh lại tag VID dựa trên label L-y và flood traffic đến VLAN-y

7. Tất cả các host trên VLAN-y tại PE2 nhận được traffic gửi đến từ PE1

> Trong trường hợp VLAN có nhiều VID, BUM traffic từ PE nguồn nên gửi kèm VID gốc trên overlay ethernet. PE đích sẽ làm nhiệm vụ ghi đè VID gốc bằng VID đích.


### 2. VLAN Bundle Service Interface

  

Đặc điểm:

- Mỗi EVI, định danh bởi một Label, có thể chứa nhiều VLANs, ánh xạ giữa VLAN và EVPN Label là n:1 trên từng PE

- Sử dụng một RT duy nhất để import/export routes cho tất cả các VLANs

- MAC-VRF của EVI chứa một bridge table duy nhất được chia sẻ giữa tất cả các VLANs => MAC phải là duy nhất giữa các VLANs
- Trong trường hợp này, 1 VLAN chỉ thể đại diện bởi 1 VID duy nhất.

- Không thể dịch VID do EVPN Label lúc này được ánh xạ tới nhiều VID's. Khi chuyển tiếp gói tin trong một VLAN, VID của gói tin overlay phải được giữ nguyên, PE đích sẽ dựa vào VID này để chuyển tiếp traffic tới PE đích tương ứng

- Ethernet Tag ID không được sử dụng (gán bằng 0)

  

Minh họa:

  

![image](/uploads/a4db31c62b6d7971ea8f6b75863ae7aa/image.png)

  

Ưu điểm:

- Sử dụng một EVI cho nhiều VLAN, khả năng mở rộng tốt

- Tiết kiệm RT

  

Nhược điểm:

- Flooding kém hiệu quả: Gói tin broadcast overlay có thể bị gửi tới PE không chứa VLAN nguồn

- Không hỗ trợ cơ chế dịch VID

- Không thể dùng chung MAC giữa các VLANs

#### Flooding BUM traffic in VLAN-Bundle


Đối với VLAN-Bundle, hình vẽ minh họa dưới đây thể hiện cả các VRF's trên từng PE. Trong ví dụ này, trên các PE's có 2 EVI tương ứng với 2 VRF x và y, mỗi VRF có duy nhất một RT và một Label tương ứng (RTx/Lx và RTy/Ly):

  

![image](/uploads/4e146c0c7d92504c74c4cef07ecdc195/image.png)

  

Luồng flooding traffic diễn ra như sau:

1. Host trên VLAN-11 gửi BUM traffic

1. PE1 nhận traffic trên VLAN-11 bên trong VRF-y

1. PE1 xác định các PE cần flood traffic đến dựa trên RT-y và L-y, trong trường hợp này là cả PE2 và PE3

1. PE1 flood traffic cho PE2 và PE3

1. PE2 nhận traffic từ PE1. Nó dựa vào VID trên overlay ethernet frame để phát hiện ra BUM traffic thuộc về VLAN-11 => Nó flood traffic tới tất cả các host trên VLAN này

1. PE3 cũng nhận traffic từ PE1, tuy nhiên trên VRF-y lại không có cấu hình VLAN11, bởi vậy nó sẽ drop traffic này

  

Hành vi drop BUM traffic tại PE3 thể hiện sự thiếu hiệu quả của VLAN Bundle khi một label được ánh xạ tới nhiều VLAN. Do mỗi PE không biết các PE khác chứa những VLAN nào, kết quả là nó flood traffic tới cả những PE không cần thiết.

  

### 3. VLAN-Aware Bundle Service Interface

  

Đặc điểm:

- Mỗi EVI chứa nhiều VLANs, mỗi VLAN tương ứng với một ethernet segment, tạo ra ánh xạ 1:1 giữa VLAN và EVPN Label trên từng PE

- Sử dụng một RT duy nhất để import/export routes cho tất cả các VLANs

- Mỗi MAC-VRF có nhiều bridge tables, mỗi bảng chứa thông tin chuyển tiếp cho một VLAN
- Trong trường hợp VLAN đại diện bởi  1 VID, PE không yêu cầu phải dịch VID, traffic tunnel cần mang theo VID ở overlay frame để PE đích xác định được VLAN và forward đến đúng các CE trong dải. Ethernet Tag ID phải set bằng VID đó trên các bản tin route quảng bá nhằm quảng bá route độc lập trong 1 VLAN của 1 EVI. PE nguồn có thể quảng bá label MPLS hoặc VNI trong bản tin route EVPN type 2. Label MPLS hoặc VNI này đại diện cho 1 EVI hoặc đại diện cho 1 cặp EVI và Ethernet Tag ID.
- Trong trường hợp VLAN đại diện bởi nhiều VID, PE yêu cầu phải dịch VID, traffic tunnel cần mang theo VID nguồn. Ethernet Tag ID phải set bằng VID tiêu chuẩn trên các bản tin route quảng bá nhằm quảng bá route độc lập trong 1 VLAN tiêu chuẩn của 1 EVI. Label MPLS hoặc VNI lúc này sẽ đại diện cho 1 cặp EVI và Ethernet Tag ID. Vì vậy, khi PE nhận gói tin tunnel, nó có thể xác định được bảng bridge tương ứng và thực hiện việc dịch VID gốc sang VID local của PE đích. 
- Vậy VID tiêu chuẩn là gì ? Theo tài liệu của Juniper có 2 cách để dịch VID: 1 là VLAN mapping, 2 là normalization. 
	- VLAN mapping là cơ chế sử dụng 2 VLAN tag liên tiếp (Q-in-Q). Ví dụ: PE1 có VID nội bộ là A, PE2 có VID nội bộ là B. Traffic từ PE1 sang PE2 trước khi đóng tunnel, sẽ được đóng 2 VLAN tag: tag A trước, tag B sau. Đến PE2, gói tin sẽ được swap 2 vlan tag cho nhau và forward vào các CE thuộc dải vlan B.
	- Normalization là cơ chế sử dụng VLAN tiêu chuẩn và VID nội bộ trên PE. Ví dụ: PE1 có VID nội bộ là A, PE2 có VID nội bộ là B. VID tiêu chuẩn là C được cấu hình ở bridge domain trong EVI ở cả 2 PE. Traffic vào PE1 được đóng tag A. VID A đã gắn vào 1 bridge domain định danh bởi VID tiêu chuẩn C theo cấu hình của người quản trị. Trong bảng bridge domain này, PE1 tìm được route đến PE2 dựa trên địa chỉ MAC. Traffic sau đó vẫn giữ VID A và đóng tunnel gửi cho PE2. PE2 nhận traffic từ PE1, dựa trên MPLS label hoặc VNI tìm ra bridge table duy nhất. PE2 thấy VLAN tag A trong frame gốc khác khác VID nội bộ đang gắn vào bridge domain đó, PE2 sẽ thực hiện việc dịch VID từ A sang B và chuyển traffic vào CE tương ứng.

  

Minh họa:

  

![image](/uploads/9c175723b8b004f4a8109c125da9bc77/image.png)

  

Ưu điểm:

- Sử dụng một EVI để quản lý nhiều VLAN => Quản lý cấu hình thuận tiện

- Tiết kiệm RT

- Hỗ trợ dịch VID

- Flooding hiệu quả (tương tự VLAN-based)

Bảng so sánh các đặc điểm của từng loại EVPN service  


Attribute | VLAN-Based | VLAN Bundle | VLAN-Aware |
---------------------------|------------|-------------|------------|
VLAN:EVI ratio | 1:1 | N:1 | N:1 |
Route target | Per-VLAN | Per-VRF | Per-VLAN |
VLAN normalization | Yes | No | Yes |
Overlapping MAC addressing | Yes | No | Yes |


  
#### Flooding BUM traffic in VLAN-Aware Bundle

  

Dưới đây là hình vẽ minh họa flooding trong VLAN-Aware. Có thể thấy trên từng PE, mỗi VID được ánh xạ tới một EVPN label tương ứng:

  

![image](/uploads/c913be86e1c45136ed79bc72929ee24c/image.png)

  

Luồng flooding traffic diễn ra như sau:

1. Host trên VLAN-11 gửi BUM traffic

2. PE1 nhận được traffic từ VLAN-11 bên trong VRF-y

3. PE1 thực hiện ánh xạ VLAN-11 sang label L11, sau đó xác định các PE cần flood traffic đến dựa trên RT-y và label L11. Do trong cặp RT-y và Label 11 chỉ có duy nhất PE2 nằm trong bridge domain (VLAN 11) nên trong trường hợp này chỉ có PE2 thỏa mãn

4. PE1 flood traffic cho PE2

5. PE2 nhận traffic từ PE1, nó sử dụng ánh xạ label L11:VID để xác định ra VID tương ứng với VLAN11, và flood gói tin cho các host nằm trong VLAN 11

6. Tất cả các host trên VLAN11 tại PE2 nhận được traffic gửi đến từ PE1

## III. Sự khác biệt giữa 3 loại VLAN service

### 1. Khác biệt cơ chế bỏ, giữ VLAN tag

Loại VLAN | VLAN-Based | VLAN Bundle | VLAN-Aware |
-|-|-|-|
Single VID | Có thể bỏ hoặc giữ VID gốc. Trường hợp bỏ VID gốc, PE đích chèn thêm VID tương ứng và chuyển tiếp cho CE. Trường hợp giữ VID gốc, PE đích chuyển tiếp thẳng traffic cho CE.  | Giữ VID gốc. Không hỗ trợ dịch VID. PE đích chuyển tiếp thẳng traffic cho CE. | Giữ VID gốc. PE đích chuyển tiếp thẳng traffic cho CE. |
Multi VID | Có thể bỏ hoặc giữ VID gốc. Trường hợp bỏ VID gốc, PE đích chèn thêm VID tương ứng và chuyển tiếp cho CE. Trường hợp giữ VID gốc, PE đích thực hiện dịch VID (nếu cần) rồi chuyển tiếp traffic cho CE.  | Không hỗ trợ. | Giữ VID gốc. PE đích thực hiện dịch VID (nếu cần) rồi chuyển tiếp traffic cho CE. |

### 2. Khác biệt cơ chế quảng bá route

Loại VLAN | VLAN-Based | VLAN Bundle | VLAN-Aware |
-|-|-|-|
Single VID | Ethernet Tag ID set bằng 0.  | Ethernet Tag ID set bằng 0. | Ethernet Tag ID chứa VID tương ứng nhằm quảng bá route EVPN đến từng EVI hoặc cặp EVI + Ethernet Tag ID. Một MPLS label/VNI đại diện cho 1 EVI + Ethernet Tag ID |
Multi VID | Ethernet Tag ID set bằng 0.  | Không hỗ trợ. | Ethernet Tag ID chứa VID tiêu chuẩn (được cấu hình bởi người quản trị) nhằm quảng bá route EVPN đến từng cặp EVI + Ethernet Tag ID. Một MPLS label/VNI đại diện cho 1 EVI + Ethernet Tag ID.  |

## IV. Q&A


- Nếu chỉ cần dùng Label là đủ thì tác dụng thực sự của ethernet tag là gì?
-> Chỉ sử dụng label (MPLS/VNI) là không đủ trong mode vlan-aware bundle. Ethernet tag sử dụng để quảng bá route cho duy nhất 1 cặp EVI + Ethernet Tag ID.

- Trong VLAN-aware service, "Normalized Ethernet Tag ID" được nhắc đến trong RFC là gì? Nếu PE nguồn và đích quảng bá chung một ethernet tag, làm thế nào để thống nhất giá trị đó, khi mà VID là khác nhau? Nếu ngược lại thì làm thế nào để PE đích biết cần import route vào bridge table nào, khi nó không biết ethernet tag đại diện cho VLAN nào trên PE nguồn?
-> "Normalized Ethernet Tag ID" là Ethernet Tag ID chứa VID tiêu chuẩn. VID tiêu chuẩn được cấu hình bởi nhà cung cấp dịch vụ EVPN.

- Khả năng phối hợp giữa các vendor trong từng loại vlan service là thế nào? Hành vi dịch VID và import route dựa trên ethernet tag có khác nhau giữa chúng hay không?
-> Mỗi vendor thường hỗ trợ 1 hoặc vài loại vlan service, cách implement cũng không hoàn toàn giống nhau sẽ gây khó khăn trong quá trình tích hợp, thậm chí sẽ không làm việc được với nhau. Hành vi dịch VID là thay đổi từ VID này sang VID kia, khác với cơ chế import route dựa trên ethernet tag id. 2 cơ chế này chỉ tồn tại trong usecase vlan-aware bundle sử dụng VLAN đại diện bởi nhiều VID.
  
> Để tránh những xung đột có thể xảy ra khi triển khai các loại vlan service, nên triển khai trường hợp đơn giản nhất: **Sử dụng VLAN-based với VID là giống nhau giữa VLAN nguồn và VLAN đích**.


## V. References

- https://tools.ietf.org/html/rfc7432#section-6
- https://www.juniper.net/documentation/en_US/junos-space-apps/connectivity-services-director2.0/topics/concept/vlan-normalization-ethernet-services.html
- `Juniper QFX10000 Series Ebook`
- https://www.juniper.net/documentation/en_US/junos/information-products/pathway-pages/junos-sdn/evpn-vxlan.pdf
