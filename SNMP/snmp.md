# Simple Network Management Protocol

Giao thức quản lý mạng là giao thức được sử dụng để truyền tải thông tin quản lý
giữa đối tượng được quản lý và các trạm quản lý mạng. Tại trạm quản lý, quản trị
viên đưa ra các yêu cầu đến các đối tượng được quản lý và nhận lại các thông tin
được yêu cầu từ chúng.

Về mặt giám sát mạng, giao thức quản lý mạng là phương tiện để gửi các yêu cầu
thu thập thông tin về tình trạng và hoạt động xảy ra trên mạng từ thiết bị mạng.
Hai giao thức thường được dùng phổ biến hiện nay là: giao thức quản lý mạng đơn
giản SNMP và giao thức thông tin quản lý chung CMIP. Trong đó, giao thức quản lý
mạng SNMP thường được sử dụng phổ biến hơn giao thức CMIP trong các hệ thống
quản lý cho mạng công cộng và mạng thương mại. Thông qua các câu lệnh, giao thức
SNMP thực hiện quá trình thu thập thông tin và đặt các bẫy cảnh báo cho thiết
bị. Các tham số truy nhập qua SNMP được nhóm vào trong các bảng cơ sở thông tin
quản lý MIB. CMIP cũng thực hiện quá trình thu thập và cài đặt tham số tương tự
như SNMP nhưng cho phép nhiều kiểu điều hành hơn và vì vậy cũng phức tạp hơn
SNMP.

## Overview

SNMP là giao thức quản lý mạng chuẩn của cộng đồng Internet cho phép quản trị
viên mạng quản lý hiệu suất mạng, thực hiện giám sát, điều khiển, tìm và giải
quyết các sự cố mạng cũng như lập kế hoạch phát triển mạng được quản lý.

SNMP là một giao thức lớp ứng dụng và sử dụng UDP tại lớp truyền vận, cụ thể
giao thức sử dụng cổng 161 và 162 trao đổi các thông tin quản lý giữa các thực
thể quản lý cung cấp các dịch vụ quản lý mạng để giám sát và điều khiển các
thiết bị mạng. SNMP sử dụng giao thức UDP bởi 2 đặc điểm:

- Hướng không kết nối: Giao thức không cần thiết lập kết nối giữa người nhận và
  người gửi.
- Best-effort: Đây là giao thức không tin cậy nó liên tục gửi dữ liệu đến người
  nhận mà không cần chờ phản hồi xác nhận.

Sử dụng UDP một phần để đơn giản hóa việc triển khai SNMP và vì không kết nối
thường là chế độ ưu tiên cho các ứng dụng quản lý mà cần phải nói chuyện với
nhiều đối tượng cùng lúc.

SNMP dựa trên giao thức phản hồi yêu cầu không đồng bộ được tăng cường với tính
năng thăm dò theo hướng trap. SNMP làm việc theo 2 hướng: poll và trap

- **Poll**: Thăm dò ở đây chính việc chủ động hoặc theo yêu cầu gửi các yêu cầu
  từ Manager (bộ phận quản lý) đến các Agent (đối tượng bị quản lý). SNMP mang đặc
  điểm không đồng bộ định tính ở đây chính là giao thức không cần đợi phản hồi
  trước khi gửi các thông báo khác.

- **Trap**: Khi có một sự kiện bất thường hay một tham số nào đó vượt ngưỡng
  Agent sẽ chủ động gửi thông báo đến Manager mà ko phải chờ đến khi Manager thực
  hiện tính năng poll.

## SNMP Architecture

Kiến trúc của SNMP bao gồm hai loại thiết bị phần cứng cơ bản:

- Các nút được quản lý: Các nút thông thường trên mạng đã được trang bị phần
  mềm để cho phép chúng được quản lý bằng SNMP. Nói chung, đây là các thiết bị
  TCP/IP thông thường; chúng cũng đôi khi được gọi là thiết bị được quản lý. Các
  thiết bị được quản lý ở đây có thể là router, bridge, hub, switch, …

- Trạm quản lý mạng (NMS): Một thiết bị mạng được chỉ định chạy phần mềm đặc
  biệt để cho phép nó quản lý các nút được quản lý thường xuyên được đề cập ở
  trên. Một hoặc nhiều NMS phải có trên mạng, vì những thiết bị này là những thiết
  bị thực sự “chạy” SNMP.

![image](/uploads/e864fc4bae1cb2b7ac1db2bd1b4d7ea1/image.png)

Mỗi thiết bị tham gia quản lý mạng bằng SNMP chạy một phần mềm, thường được gọi
là thực thể SNMP. Thực thể SNMP chịu trách nhiệm thực hiện tất cả các chức năng
khác nhau của giao thức SNMP. Mỗi thực thể bao gồm hai thành phần phần mềm
chính. Những thành phần nào bao gồm thực thể SNMP trên một thiết bị tất nhiên
phụ thuộc vào việc thiết bị đó là một nút được quản lý hay một trạm quản lý
mạng.

Thực thể SNMP trên một trạm quản lý mạng bao gồm:

- SNMP Manager: Một chương trình phần mềm thực hiện giao thức SNMP, cho phép NMS
  thu thập thông tin từ các nút được quản lý và gửi hướng dẫn cho chúng.
- Ứng dụng SNMP: Một hoặc nhiều ứng dụng phần mềm cho phép người quản trị mạng
  sử dụng SNMP để quản lý mạng.

SNMP bao gồm một số lượng nhỏ các trạm quản lý mạng (NMS) tương tác với các
thiết bị TCP/IP thông thường được gọi là các nút được quản lý. Trình quản lý
SNMP trên NMS và các Agent trên các nút được quản lý thực hiện giao thức SNMP và
cho phép trao đổi thông tin quản lý mạng. Các ứng dụng SNMP chạy trên NMS và
cung cấp giao diện cho quản trị viên con người, đồng thời cho phép thu thập
thông tin từ MIB tại mỗi SNMP Agent.

Thực thể SNMP trên một nút được quản lý bao gồm các phần tử và cấu trúc sau:

- SNMP Agent: Một chương trình phần mềm triển khai giao thức SNMP và cho phép
  một nút được quản lý cung cấp thông tin cho NMS và chấp nhận các lệnh từ nó.
- Cơ sở thông tin quản lý SNMP (MIB): Xác định các loại thông tin được lưu trữ
  về nút có thể được thu thập và sử dụng để điều khiển nút được quản lý. Thông tin
  được trao đổi bằng SNMP có dạng các đối tượng từ MIB.

## Cơ sở thông tin quản lý MIB

Mỗi thiết bị chịu sự quản lý có thể có cấu hình, trạng thái và thông tin thống
kê định nghĩa chức năng và khả năng vận hành của thiết bị. Thông tin này rất đa
dạng, có thể bao gồm việc thiết lập chuyển mạch phần cứng, những giá trị khác
nhau lưu trữ trong các bảng ghi nhớ dữ liệu, bộ hồ sơ hoặc các trường thông tin
trong hồ sơ lưu trữ ở các file và những biến hoặc thành phần dữ liệu tương tự.
Nhìn chung, những thành phần dữ liệu này được coi là Cơ sở thông tin quản lý
của thiết bị chịu sự quản lý. Xét riêng, mỗi thành phần dữ liệu biến đổi được
coi là một đối tượng bị quản lý và bao gồm tên, một hoặc nhiều thuộc tính và một
tập các hoạt động thực hiện trên đối tượng đó.

**_Cấu trúc của thông tin quản lý MIB_**

Thông tin quản lý hệ thống SMI (System Management Information) định nghĩa một cơ
cấu tổ chức chung cho thông tin quản lý. SMI nhận dạng các kiểu dữ liệu trong
MIB và chỉ rõ cách thức miêu tả và đặt tên các tài nguyên trong cơ sở dữ liệu
thông tin quản lý MIB.
Những đặc tả của các đối tượng SNMP quản lý được SMI thực hiện thông qua ngôn
ngữ mô tả ASN.1 macro, cụ thể SMI sử dụng kiểu OBJECT-TYPE, cung cấp mô hình
chính thức định nghĩa các đối tượng và bảng của các đối tượng trong MIB bởi các
trường:

- SYNTAX: Định nghĩa dữ liệu tóm tắt xác định cấu trúc dữ liệu tương ứng
  với đối tượng đó. Trường yêu cầu bắt buộc.
  EX: INTEGER, OCTET STRING, OBJECT IDENTIFIER, BITS, IpAddress, Counter,
  Gauge32, Unsigned32, TimeTick.
- UNITS: Định nghĩa đơn vị của đối tượng đó.
- ACCESS: Xác định mức độ truy cập cho đối tượng. Trường yêu cầu bắt buộc. EX:
  not-accessible, accessible-for-notify, read-only, read-write, read-create.
- STATUS: Cho biết định nghĩa này là hiện tại hay lịch sử . Trường yêu cầu bắt
  buộc. EX: current,obsolete,deprecated.
- DESCRIPTION: Chứa định nghĩa mô tả về đối tượng. Trường yêu cầu bắt buộc.
- REFERENCE: Chứa tham chiếu chéo văn bản đến một số tài liệu khác, hoặc một
  thông tin khác
- INDEX: Xác định thông tin nhận dạng cho các đối tượng cột trực thuộc
  đối tượng đó.
- DEFVAL: Định nghĩa các giá trị mặc định có thể chấp nhận

ifDescr định nghĩa trong MIB

```sh
ifDescr OBJECT-TYPE
    SYNTAX  DisplayString (SIZE (0..255))
    ACCESS  read-only
    STATUS  mandatory
    DESCRIPTION  "A textual string containing information about the interface.
    This string should include the name of the manufacturer, the product name
    and the version of the hardware interface."
     ::= { ifEntry 2 }
```

**_Nhận dạng đối tượng (OID)_**

Các đối tượng quản lý trong môi trường SNMP được sắp xếp theo cấu trúc hình cây
có thứ bậc. Lá của cây là đối tượng quản lý thực, mỗi thành phần trong đối tượng
này biểu thị cho tài nguyên, sự hoạt động hoặc các thông tin liên quan được quản
lý. Mỗi dạng đối tượng liên kết trong một MIB là một nhận diện của kiểu ASN.1
OBJECT IDENTIFIER. Việc nhận dạng phục vụ cho việc đặt tên của đối tượng và cũng
phục vụ cho việc nhận diện cấu trúc của các dạng đối tượng.

![image](/uploads/2e0bc68b47b8b3912eabcadc61978c0f/image.png)
OID gồm một dãy có thứ tự gồm các số không âm để xác định một object (đối tượng)
cụ thể, mỗi số trong dãy được gọi là số nhận dạng phụ.
Ví dụ:

- Tên thiết bị được gọi là sysName, OID là `1.3.6.1.2.1.1.5`
- Địa chỉ Mac Address của một port được gọi là ifPhysAddress, OID là
  `1.3.6.1.2.1.2.2.1.6`

- Số byte đã nhận trên một port được gọi là ifInOctets, OID là
  `1.3.6.1.2.1.2.2.1.10`

Một object có thể có nhiều giá trị cùng loại. Chẳng hạn một thiết bị có thể có
nhiều tên, có nhiều địa chỉ Mac. Một object chỉ có một OID, vì vậy để chỉ ra các
giá trị khác nhau của cùng một object thì ta dùng thêm một phân cấp nữa gọi là
sub-id.

Ví dụ:

- Tên thiết bị được gọi là sysName, OID là `1.3.6.1.2.1.1.5`; nếu thiết bị có 2
tên thì chúng sẽ được gọi là sysName.0 & sysName.1 và có OID lần lượt là
`1.3.6.1.2.1.1.5.0` & `1.3.6.1.2.1.1.5.1`

- Địa chỉ Mac address được gọi là ifPhysAddress, OID là `1.3.6.1.2.1.2.2.1.6`; 
nếu thiết bị có 2 mac address thì chúng sẽ được gọi là ifPhysAddress.0 &
ifPhysAddress.1 và có OID lần lượt là `1.3.6.1.2.1.2.2.1.6.0` &
`1.3.6.1.2.1.2.2.1.6.1`

Các object có thể có nhiều giá trị hoặc một giá trị thì luôn luôn được viết dưới
dạng có phân cấp con sub-id. Ví dụ một thiết bị dù chỉ có một tên thì nó vẫn
phải viết là sysName.0 hay `1.3.6.1.2.1.1.5.0`. Đối với các object có nhiều giá
trị thì các chỉ số của phân cấp con không nhất thiết phải liên tục hay bắt đầu
từ 0. Ví dụ một thiết bị có 2 mac address thì có thể chúng được gọi là
ifPhysAddress.23 và ifPhysAddress.125645.

OID của các object phổ biến có thể được chuẩn hóa, hoặc tự định nghĩa. Để lấy
một thông tin có OID đã chuẩn hóa thì ứng dụng SNMP phải gửi một bản tin SNMP có
chứa OID của object đó cho SNMP Agent, SNMP Agent khi nhận được thì nó phải trả
lời bằng thông tin ứng với OID đó.

## Mô hình giao thức

Giao thức SNMP trong mô hình chồng giao thức TCP/IP, SNMP thuộc về lớp ứng dụng
trong mô hình giao thức, nó sử dụng UDP là giao thức lớp vận chuyển trên mạng IP
như mô tả. Nhìn trên phương diện truyền thông Manager và các Agent cũng là những
người sử dụng, sử dụng một giao thức ứng dụng. Giao thức quản lý yêu cầu cơ chế
vận chuyển để hỗ trợ tương tác giữa các Agent và Manager. Manager trước hết phải
xác định được các Agent mà nó muốn liên lạc. Có thể xác định được ứng dụng Agent
bằng địa chỉ IP của nó và cổng UDP được gán cho nó. Cổng UDP 161 được dành riêng
cho các Agent SNMP. Manager đóng gói bản tin SNMP vào một tiêu đề UDP. Tiêu đề
này chứa cổng nguồn, địa chỉ IP đích và cổng 161. Một thực thể IP tại chỗ sẽ
chuyển giao gói UDP tới hệ thống bị quản lý. Tiếp đó, một thực thể UDP tại chỗ
sẽ chuyển phát nó tới các Agent. Tương tự như vậy, lệnh TRAP cũng cần xác định
những Manager mà nó cần liên hệ. Chúng sử dụng địa chỉ IP cũng như cổng UDP dành
cho SNMP Manager, đó là cổng 162.

![image](/uploads/5dee6b010b8e3657ea3450bea4a8b753/image.png)

## Phương thức hoạt động

SNMP sử dụng 3 lệnh cơ bản là Read, Write, Trap và một số lệnh tùy biến để quản
lý thiết bị.

- Read: Được SNMP dùng để đọc thông tin từ thiết bị. Các thông tin này được cung
  cấp qua các biến SNMP lưu trữ trên thiết bị và được thiết bị cập nhật.
- Write: Được SNMP dùng để ghi các thông tin điều khiển lên thiết bị bằng cách
  thay đổi giá trị các biến SNMP.
- Trap: Dùng để nhận các sự kiện gửi từ thiết bị đến SNMP. Mỗi khi có một sự
  kiện xảy ra trên thiết bị một lệnh Trap sẽ được gửi tới NMS.

SNMP điều khiển và theo dõi thiết bị bằng cách thay đổi hoặc thu thập thông tin
qua các biến giá trị lưu trên thiết bị. Các Agent cài đặt trên thiết bị tương
tác với những chip điều khiển hỗ trợ SNMP để lấy nội dung hoặc viết lại nội
dung.

SNMP sử dụng các phương thức khác nhau được mô tả trong bảng để trao đổi các bản
tin với các mục đích khác nhau gửi Manager và các Agent cụ thể. Mỗi bản tin đều
có chứa OID để cho biết đối tượng mang trong nó là gì. OID trong Get cho biết nó
muốn lấy thông tin của đối tượng nào. OID trong Response cho biết nó mang giá
trị của đối tượng nào. OID trong Set chỉ ra nó muốn thiết lập giá trị cho đối
tượng nào. OID trong Trap chỉ ra nó thông báo sự kiện xảy ra đối với đối tượng
nào.

| Phương thức | Mô tả                                                                                                                                                                                        |
| ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| GET         | Bản tin yêu cầu được gửi bởi Manager đến Agent để lấy một hoặc nhiều giá trị từ thiết bị được quản lý dựa trên các OID.                                                                      |
| GETNEXT     | Bản tin yêu cầu được gửi từ Manager đến Agent để yêu cầu cung cấp thông tin nằm kế tiếp OID đó trong bảng MIB.                                                                               |
| SET         | Bản tin được gửi bởi Manager đến Agent để thay đổi giá trị được giữ bởi một biến trên Agent, có thể được sử dụng để kiểm soát thông tin cấu hình hoặc sửa đổi trạng thái của máy chủ từ xa.  |
| GETBULK     | Tương tự như GETNEXT ngoại trừ vấn đề liên quan tới số lượng dữ liệu được lấy ra, nó cho phép Agent gửi lại Manager dữ liệu liên quan tới nhiều đối tượng thay vì từng đối tượng bị quản lý. |
| RESPONSE    | Bản tin được gửi bởi Agent tới Manager được dùng để phản hồi lại yêu cầu GET/SET ở trên.                                                                                                     |
| TRAP        | Bản tin do Agent tự động gửi tới Manager khi có sự kiện xảy ra đối với một đối tượng nào đó.                                                                                                 |
| INFORM      | Bản tin được dùng để trao đổi giữa các Manager về các cảnh báo và sự kiện.                                                                                                                   |

**_Get_**

Bản tin Get được Manager gửi đến Agent để lấy một thông tin nào đó. Trong Get có
chứa OID của object muốn lấy. Ví dụ : Muốn lấy thông tin tên của thiết bị 1 thì
Manager gửi bản tin Get OID = `1.3.6.1.2.1.1.5` đến thiết bị 1, tiến trình SNMP
Agent trên thiết bị đó sẽ nhận được bản tin và tạo bản tin trả lời. Trong một
bản tin Get có thể chứa nhiều OID, nghĩa là dùng một Get có thể lấy về cùng lúc
nhiều thông tin.

**_GetNext_**

Phương thức này tương tự như Get, tuy nhiên tuỳ thuộc vào Agent trong khoản mục
kế tiếp của MIB. Các biến được lưu trong thiết bị và được coi như đối tượng bị
quản lý. Vì vậy, phương thức GetNext mở rộng các biến và được đọc theo tuần tự.
Trong một file MIB bao gồm nhiều OID được sắp xếp thứ tự nhưng không liên tục,
nếu biết một OID thì không xác định được OID kế tiếp. Do đó ta cần GetNext để
lấy về giá trị của OID kế tiếp. Nếu thực hiện GetNext liên tục thì ta sẽ lấy
được toàn bộ thông tin của Agent.

**_GetBulk_**

Chức năng của phương thức GetBulk tương tự như GetNext ngoại trừ vấn đề liên
quan tới số lượng dữ liệu được lấy ra. GetBulkRequest cho phép Agent gửi lại
Manager dữ liệu liên quan tới nhiều đối tượng thay vì từng đối tượng bị quản lý.
Như vậy, GetBulk có thể giảm bớt lưu lượng truyền dẫn và các bản tin đáp ứng
thông báo về các điều kiện vi phạm.

**_Response_**

Mỗi khi SNMP Agent nhận được các bản tin Get, GetNext hay Set thì nó sẽ gửi lại
bản tin Response để trả lời. Trong bản tin Response có chứa OID của object được
yêu cầu và giá trị của object đó.

![image](/uploads/e2a1f7d86529b355e8ebbb6b033d0cc3/image.png)

![image](/uploads/d89105ae85336a1ab4bbd88c7cb5e42b/image.png)

Các bản tin trao đổi trong SNMP chứa đơn vị giao thức PDU (Protocol Data Unit).
Cấu trúc của các bản tin được mô tả bao gồm các trường:

- Version: chỉ định phiên bản của giao thức SNMP.
- Community: một chuỗi mật khẩu xác nhận cho cả tiến trình lấy và thay đổi dữ
  liệu.
- PDU type: Chỉ định loại bản tin PDU được sử dụng.
- Request ID: chỉ định một ID duy nhất cho mỗi yêu cầu từ Manager được dùng để
  phân biệt các bản tin. Request ID của bản tin Response tương ứng với Request
  ID của bản tin yêu cầu nhận được.
- Non repeaters: Báo cho Agent biết có bao nhiêu đối tượng có thể trả lời như
  một yêu cầu Get.
- Max repetitions: Chỉ định bao nhiêu yêu cầu GetNext cho các đối tượng còn lại.
- Error status: Chỉ định trạng thái lỗi xảy ra khi xử lý một yêu cầu. Trường này
  chỉ có trên bản tin Response trả về từ Agent. Một số lỗi có thể gặp khi nhận
  bản tin Response được mô tả trong bảng.
- Error index: Khi trường error-status khác 0, giá trị error-index thể hiện biến
  (đối tượng) trong danh sách liên kết biến gây ra lỗi. Biến đầu tiên trong danh
  sách có chỉ mục là 1. Biến thứ hai có chỉ mục là 2.
- Variable bindings: chỉ định một dãy bao gồm tên biến yêu cầu và giá trị tương
  ứng phản hồi từ yêu cầu.

| Loại PDU | Giá trị |
| -------- | ------- |
| Get      | 0       |
| GetNext  | 1       |
| Response | 2       |
| Set      | 3       |
| GetBulk  | 4       |
| Inform   | 5       |
| Trap     | 6       |
| Report   | 7       |

Một số mã lỗi trả về từ bản tin Response

| Mã lỗi              | Mô tả                                                               |
| ------------------- | ------------------------------------------------------------------- |
| noError             | Không có lỗi xảy ra                                                 |
| tooBig              | Kích cỡ của bản tin Response quá lớn để gửi                         |
| noSuchname          | Không tìm thấy tên của đối tượng yêu cầu                            |
| badValue            | Giá trị của đối tượng yêu cầu ko phù hợp với cấu trúc cho đối tượng |
| readOnly            | Giá trị của đối tượng chỉ có thể đọc                                |
| noAccess            | Đối tượng không được cho phép truy cập thay đổi                     |
| wrongType           | Giá trị yêu cầu Set sai kiểu giá trị                                |
| wrongLength         | Chiều dài giá trị yêu cầu Set không đúng                            |
| wrongEncoding       | Mã hóa cho giá trị yêu cầu Set không đúng                           |
| wrongValue          | Giá trị yêu cầu Set không phù hợp                                   |
| noCreation          | Không thể tạo cho yêu cầu Set                                       |
| inconsistentValue   | Giá trị cho yêu cầu Set không nhất quán                             |
| resourceUnavailable | Tài nguyên yêu cầu không có sẵn                                     |
| commitFailed        | Cam kết cho yêu cầu Set thất bại                                    |
| authorizationError  | Lỗi xác thực                                                        |
| notWritable         | Không thể viết                                                      |

**_Set_**

Bản tin câu lệnh được gửi đi từ Manager tới Agent như hai phương thức trên. Set
tìm kiếm các thông tin mở rộng trong bảng MIB và yêu cầu Agent đặt giá trị cho
các đối tượng quản lý hoặc các đối tượng chứa trong câu lệnh. Sự thành công của
câu lệnh này phụ thuộc vào một số yếu tố gồm sự tồn tại của các đối tượng bị
quản lý và các phương thức truy nhập.

Lưu ý: Chỉ những đối tượng có quyền `READ_WRITE` mới có thể thay đổi được giá trị.
Ví dụ:

- Có thể đặt lại tên của một máy tính hay router bằng phần mềm SNMP Manager,
  bằng cách gửi bản tin Set có OID là `1.3.6.1.2.1.1.5.0` (sysName.0) và có giá
  trị là tên mới cần đặt.
- Có thể tắt một port trên switch bằng phần mềm SNMP Manager, bằng cách gửi bản
  tin có OID là `1.3.6.1.2.1.2.2.1.7` (ifAdminStatus) và có giá trị là 2 .

![image](/uploads/bea6058da1ce1af7d2b608649d7ea3fd/image.png)

Gói tin yêu cầu của phương thức Set có cấu trúc tương tự như các phương thức
trên

![image](/uploads/4ab61f6745849419fa67b4f18aa9d2d2/image.png)
**_Trap_**

Phương thức được sử dụng để gửi các bản tin thông báo được gửi từ Agent đến
Manager khi có cảnh báo hay một sự kiện nào đó được tạo bởi thiết bị.

![image](/uploads/bc63b4f4299ce191ce652e93ef4d8329/image.png)

Cấu trúc bản tin Trap của phiên bản SNMPv2 và SNMPv3 tương tự với các phương
thức trên, tuy nhiên với phiên bản SNMPv1 các trường khác được đóng gói trong
gói tin PDU mang những thông tin phục vụ chức năng được nêu sau đây.

![image](/uploads/f783e474b5face612486667c46ded3f6/image.png)

![image](/uploads/04ce77674c315b368c59ea5927d2f63b/image.png)

- Enterprise: Chỉ định tên thiết bị tạo ra trap
- Agent addr: Chỉ định địa chỉ IP của nguồn trap
- Generic trap: Chỉ định loại trap được tạo. Ví dụ: coldStart, warmStart,
  linkDown, linkUp, authenticationFailure, agpNeighborLoss,…
- Specific trap: Chỉ định thông tin trap của một Enterprise
- Time stamp: Chỉ định thời gian trap được tạo

**_Inform_**

Phương thức Inform cung cấp khả năng hỗ trợ các Manager bố trí theo cấu hình
phân cấp. Phương thức này cho phép một Manager trao đổi thông tin với các
Manager khác. Các cảnh báo và sự kiện được gửi đi trong Inform để phát hiện và
khởi tạo lại các tuyến truyền bản tin. Một trạm quản lý có thể thông tin tới các
trạm quản lý lân cận biết các điều kiện quan trọng trong vùng quản lý.

![image](/uploads/823a9aeec4f78014cf64391f4679bfb2/image.png)

Phương thức Inform chỉ xuất hiện trong phiên bản SNMPv2 và SNMPv3 có cấu trúc
giống với phương thức Trap.

## Phiên bản SNMP

- SNMPv1 (1991) là phiên bản ban đầu của SNMP được viết lại từ RFC 1067 được xác
  định trong RFC 1155 và 1157, cung cấp các chức năng quản lý mạng tối thiểu.
  SNMPv1 cung cấp xác thực dựa trên chuỗi community, có tính bảo mật thấp. Ngoài
  ra, số lượng mã lỗi hạn chế được trả về trong các gói.
- SNMPv2 (1993) trở thành tiêu chuẩn quản lý mạng đơn giản thay thế SNMPv1 được
  định nghĩa trong RFC 1901, RFC 1905, RFC 1906, RFC 2578. SNMPv2 bổ sung một số
  vấn đề mà SNMPv1 còn thiếu như mã lỗi, bản tin Trap và bổ sung phương thực và
  xác thực vẫn sử dụng chuỗi community.
- SNMPv3 (1997) ra đời nhằm tương thích với các giao thức đa phương tiện trong
  quản lý mạng, những cải tiến, bổ sung nhằm làm hoàn thiện hơn SNMPv3 được
  trình bày trong các tài liệu RFC 2570 - RFC 2576 (1999) và RFC 3410 – RFC 3418
  (2002). Mục đích chính của SNMPv3 là hỗ trợ kiến trúc theo kiểu module để có
  thể dễ dàng mở rộng. Theo cách này, nếu các giao thức bảo mật mới được mở rộng
  chúng có thể được SNMPv3 hỗ trợ như là các module riêng, sử dụng MAC dựa trên
  Hash với MD5 hoặc SHA để xác thực và DES-56 cho quyền riêng tư.

## SNMP-exporter

SNMP- exporter thu thập thông tin dựa trên giao thức SNMP bằng cách đọc tệp cấu
hình mặc định `snmp.yml` chứa thông tin OID và thông tin đăng nhập sử dụng trong
trường hợp nếu đó là SNMPv2 hoặc SNMPv3, sau đó sử dụng phương thức GETBULK để
`walk/get` thông tin từ thiết bị, các thông tin thập được tổng hợp thành các
kiểu số liệu Prometheus. Vì chỉ để phục vụ cho nhiệm vụ giám sát, thu thập số
liệu nên SNMP-exporter chỉ được xây dựng với phương thức `GETBULK` không có các
phương thức khác như SET, TRAP được trình bày ở trên.

Tệp cấu hình của SNMP–exporter rất phức tạp không sử dụng viết tay được mà cần
sử dụng một công cụ generator để tạo ra chúng dựa trên tệp mib của mỗi loại
thiết bị được cung cấp bởi nhà sản xuất. Các thông tin thu thập được hoàn toàn
phụ thuộc vào tệp mibs nên chỉ có thể được các số liệu được nhà sản xuất cung
cấp trong tệp mibs.

## References

[Advances_in_Network Management.pdf](<./1-JianguoDing-Advances_in_Network_Management_(2010).pdf>)
